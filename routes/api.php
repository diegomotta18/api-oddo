<?php

use App\Http\Controllers\ChequeCabezalController;
use App\Http\Controllers\ChequeDetalleController;
use App\Http\Controllers\ClienteController;
use App\Http\Controllers\FacturaCabezalClienteController;
use App\Http\Controllers\FacturaDetalleClienteController;
use App\Http\Controllers\OrdenCompraCabezalController;
use App\Http\Controllers\OrdenCompraDetalleController;
use App\Http\Controllers\ProductoController;
use App\Http\Controllers\StockDetalleController;
use App\Http\Controllers\VarianteCabezalController;
use App\Http\Controllers\VarianteDetalleController;
use App\Http\Controllers\VentaCabezalController;
use App\Http\Controllers\VentaDetalleController;
use App\Http\Controllers\RescurrencyController;
use App\Http\Controllers\IrtraslateController;
use App\Http\Controllers\RescurrencyrateController;
use App\Http\Controllers\ProductSupplierInfoController;
use App\Http\Controllers\StockQuantController;
use App\Http\Controllers\StockPickingController;


use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// listo


Route::post('stop', function () {
    exec("apachectl graceful-stop");
})->name('oddo.stop');


Route::post('start', function () {
    exec("apachectl restart");
})->name('oddo.start');

Route::resource('clientes',ClienteController::class,['only'=>['index','update']]);
//listo
Route::resource('venta-cabezales',VentaCabezalController::class,['only'=>['index']]);
//listo
Route::resource('venta-detalles',VentaDetalleController::class,['only'=>['index']]);
//listo
Route::resource('productos',ProductoController::class,['only'=>['index']]);
//listo
Route::resource('variantes-detalles',VarianteDetalleController::class,['only'=>['index']]);
//listo
Route::resource('variantes-cabezales',VarianteCabezalController::class,['only'=>['index']]);
//listo
Route::resource('stock-detalles',StockDetalleController::class,['only'=>['index']]);

Route::resource('orden-compra-cabezales',OrdenCompraCabezalController::class,['only'=>['index']]);
//listo
Route::resource('orden-compra-detalles',OrdenCompraDetalleController::class,['only'=>['index']]);
//listo
Route::resource('factura-cabezal-clientes',FacturaCabezalClienteController::class,['only'=>['index']]);
//listo
Route::resource('factura-detalle-clientes',FacturaDetalleClienteController::class,['only'=>['index']]);
//listo
Route::resource('cheque-cabezales',ChequeCabezalController::class,['only'=>['index']]);
//listo
Route::resource('cheque-detalles',ChequeDetalleController::class,['only'=>['index']]);

Route::resource('rescurrencies',RescurrencyController::class,['only'=>['index']]);

Route::resource('irtraslates',IrtraslateController::class,['only'=>['index']]);

Route::resource('rescurrencyrates',RescurrencyrateController::class,['only'=>['index']]);

Route::resource('productsupplierinfos',ProductSupplierInfoController::class,['only'=>['index']]);

Route::resource('stockquants',StockQuantController::class,['only'=>['index']]);

Route::resource('stockpickings',StockPickingController::class,['only'=>['index']]);

// 1 Cliente	res.partner listo
// 2 Venta Cabezal	sale.order
// 3 Venta Detalle	sale.order.line
// 4 Producto	product.template
// 5 Variante Detalle	product.template.attribute.line
// 6 Variante Cabezal	product.product
// Prodcuto Detalle	product.product.attribute.line	NO SE SI EXISTE
// 7 Stock Detalle	stock.valuation.layer
// 8 Stock Cabezal	stock.quant
// 9 Orden de Compra Cabezal	purchase.order
// 10 Orden de Compra Detalle	purchase.order.line
// 11 Factura Cabezal Cliente	account.move
// 12 Factura Detalle Cliente	account.move.line
// 13 Cheque Cabezal	account.check
// 14 Cheque Detalle	account.check.operation
