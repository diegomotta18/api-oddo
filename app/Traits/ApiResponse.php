<?php

namespace App\Traits;

use App\Transformers\InvoiceTransform;
use App\Transformers\FixedCostTransform;

use Illuminate\Support\Collection;
// use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Validator;
trait ApiResponse{

    protected function successResponse($data,$code){
        return response()->json($data,$code);
    }

    protected function errorResponse($message,$code){
        return response()->json(['error'=> $message,'code'=>$code],$code);
    }


}
