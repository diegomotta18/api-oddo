<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class PurchaseOrder extends Model
{
    use HasFactory;
    protected $table =      'purchase_orders';
    public $timestamps = false;

    protected $fillable = [
        "name",
        "origin",
        "partner_ref",
        "date_order",
        "date_approve",
        "partner_id",
        "dest_address_id",
        "currency_id",
        "state",
        "order_line",
        "notes",
        "invoice_count",
        "invoice_ids",
        "invoice_status",
        "date_planned",
        "amount_untaxed",
        "amount_tax",
        "amount_total",
        "fiscal_position_id",
        "payment_term_id",
        "product_id",
        "user_id",
        "company_id",
        "currency_rate",
        "incoterm_id",
        "picking_count",
        "picking_ids",
        "picking_type_id",
        "default_location_dest_id_usage",
        "group_id",
        "is_shipped",
        "internal_notes",
        "force_invoiced_status",
        "force_delivered_status",
        "delivery_status",
        "with_returns",
        "activity_exception_decoration",
        "activity_exception_icon",
        "activity_ids",
        "activity_state",
        "activity_user_id",
        "activity_type_id",
        "activity_date_deadline",
        "activity_summary",
        "message_is_follower",
        "message_follower_ids",
        "message_partner_ids",
        "message_channel_ids",
        "message_ids",
        "message_unread",
        "message_unread_counter",
        "message_needaction",
        "message_needaction_counter",
        "message_has_error",
        "message_has_error_counter",
        "message_main_attachment_id",
        "failed_message_ids",
        "website_message_ids",
        "message_has_sms_error",
        "message_attachment_count",
        "smart_search",
        "access_url",
        "access_token",
        "access_warning",
        "purchase_order_id",
        "display_name",
        "create_uid",
        "create_date",
        "write_uid",
        "write_date",
        "__last_update",
    ];

    public function fromDateTime($value){
        return Carbon::parse(parent::fromDateTime($value))->format("Y-d-m H:i:s");
    }
}


