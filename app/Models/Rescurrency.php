<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Rescurrency extends Model
{
    use HasFactory;

    protected $table = 'rescurrencies';

    public $timestamps = false;
    protected $fillable = [
            "name",
            "symbol",
            "rate",
            "rate_ids",
            "rounding",
            "decimal_places",
            "active",
            "position",
            "date",
            "currency_unit_label",
            "currency_subunit_label",
            "inverse_rate",
            "l10n_ar_afip_code",
            "smart_search",
            "rescurrency_id",
            "display_name",
            "create_uid" ,
            "create_date" ,
            "write_uid",
            "write_date",
            "__last_update",
    ];

    public function fromDateTime($value){
        return Carbon::parse(parent::fromDateTime($value))->format("Y-d-m H:i:s");
    }
}
