<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Irtraslate extends Model
{
    use HasFactory;

    protected $table = 'irtraslates';

    public $timestamps = false;
    protected $fillable = [
        "name",
        "res_id",
        "lang",
        "type",
        "src",
        "value",
        "module",
        "state",
        "comments",
        "transifex_url",
        "smart_search",
        "irtraslate_id",
        "display_name",
        "__last_update",
    ];

    public function fromDateTime($value){
        return Carbon::parse(parent::fromDateTime($value))->format("Y-d-m H:i:s");
    }
}
