<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class ProductTemplateAttributeLine extends Model
{
    use HasFactory;
    protected $table='product_template_attribute_lines';
    public $timestamps = false;
    protected $fillable = [
        "active",
        "product_tmpl_id",
        "attribute_id",
        "value_ids",
        "product_template_value_ids",
        "smart_search",
        "product_template_attribute_line_id",
        "display_name",
        "create_uid",
        "create_date",
        "write_uid",
        "write_date",
        "__last_update",
    ];

    public function fromDateTime($value){
        return Carbon::parse(parent::fromDateTime($value))->format("Y-d-m H:i:s");
    }
}
