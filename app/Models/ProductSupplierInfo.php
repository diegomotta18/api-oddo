<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductSupplierInfo extends Model
{
    use HasFactory;
    protected $table='product_supplier_infos';
    public $timestamps = false;
    protected $fillable = [
        "name",
        "product_name",
        "product_code",
        "sequence" ,
        "product_uom" ,
        "min_qty" ,
        "price" ,
        "company_id" ,
        "currency_id" ,
        "date_start" ,
        "date_end" ,
        "product_id" ,
        "product_tmpl_id" ,
        "product_variant_count" ,
        "delay" ,
        "last_date_price_updated" ,
        "replenishment_cost_rule_id" ,
        "net_price" ,
        "discount" ,
        "smart_search" ,
        "product_supplier_infos_id" ,
        "display_name" ,
        "create_uid" ,
        "create_date" ,
        "write_uid" ,
        "write_date" ,
        "__last_update"
    ];

    public function fromDateTime($value){
        return Carbon::parse(parent::fromDateTime($value))->format("Y-d-m H:i:s");
    }
}
