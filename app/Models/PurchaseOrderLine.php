<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class PurchaseOrderLine extends Model
{
    use HasFactory;
    protected $table =      'purchase_order_lines';
    public $timestamps = false;
    protected $fillable = [
        "name",
        "sequence" ,
        "product_qty" ,
        "product_uom_qty" ,
        "date_planned" ,
        "taxes_id" ,
        "product_uom" ,
        "product_uom_category_id" ,
        "product_id" ,
        "product_type" ,
        "price_unit" ,
        "price_subtotal" ,
        "price_total" ,
        "price_tax" ,
        "account_analytic_id" ,
        "analytic_tag_ids" ,
        "company_id" ,
        "state" ,
        "invoice_lines" ,
        "qty_invoiced" ,
        "qty_received" ,
        "qty_received_manual" ,
        "partner_id" ,
        "currency_id" ,
        "date_order" ,
        "display_type" ,
        "qty_received_method" ,
        "move_ids" ,
        "orderpoint_id" ,
        "move_dest_ids" ,
        "propagate_date" ,
        "propagate_date_minimum_delta" ,
        "propagate_cancel" ,
        "discount" ,
        "order_id" ,
        "invoice_status" ,
        "invoice_qty" ,
        "qty_to_invoice" ,
        "sale_order_id" ,
        "sale_line_id" ,
        "delivery_status" ,
        "vouchers" ,
        "qty_on_voucher" ,
        "qty_returned" ,
        "smart_search" ,
        "purchase_order_line_id" ,
        "display_name" ,
        "create_uid" ,
        "create_date" ,
        "write_uid" ,
        "write_date" ,
        "__last_update" ,
    ];

    public function fromDateTime($value){
        return Carbon::parse(parent::fromDateTime($value))->format("Y-d-m H:i:s");
    }
}
