<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class StockValuationLayer extends Model
{
    use HasFactory;

    protected $table = "stock_valuation_layers";
    public $timestamps = false;
    protected $fillable = [
        "active",
        "company_id",
        "product_id",
        "categ_id",
        "product_tmpl_id",
        "quantity",
        "uom_id",
        "currency_id",
        "unit_cost",
        "value",
        "remaining_qty",
        "remaining_value",
        "description",
        "stock_valuation_layer_id",
        "stock_valuation_layer_ids",
        "stock_move_id",
        "account_move_id",
        "smart_search",
        "stock_valuation_layer_app_id",
        "display_name",
        "create_uid",
        "create_date",
        "write_uid",
        "write_date",
        "__last_update",
    ];

    public function fromDateTime($value){
        return Carbon::parse(parent::fromDateTime($value))->format("Y-d-m H:i:s");
    }
}
