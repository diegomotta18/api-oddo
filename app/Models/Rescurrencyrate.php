<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Rescurrencyrate extends Model
{
    use HasFactory;

    protected $table = 'rescurrencyrates';

    public $timestamps = false;
    protected $fillable = [
            "name",
            "rate",
            "currency_id",
            "company_id",
            "inverse_rate",
            "smart_search",
            "rescurrencyrate_id",
            "display_name",
            "create_uid",
            "create_date",
            "write_uid",
            "write_date",
            "__last_update",
    ];

    public function fromDateTime($value){
        return Carbon::parse(parent::fromDateTime($value))->format("Y-d-m H:i:s");
    }
}
