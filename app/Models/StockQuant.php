<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class StockQuant extends Model
{
    use HasFactory;
    protected $table = "stock_quants";

    public $timestamps = false;

    protected $fillable = [
        "product_id",
        "product_tmpl_id",
        "product_uom_id",
        "company_id",
        "location_id",
        "lot_id",
        "package_id",
        "owner_id",
        "quantity",
        "reserved_quantity",
        "in_date",
        "tracking",
        "on_hand",
        "removal_date",
        "smart_search",
        "stock_quant_id",
        "display_name",
        "create_uid",
        "create_date",
        "write_uid",
        "write_date",
        "__last_update",
    ];

    public function fromDateTime($value){
        return Carbon::parse(parent::fromDateTime($value))->format("Y-d-m H:i:s");
    }

}
