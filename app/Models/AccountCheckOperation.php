<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class AccountCheckOperation extends Model
{
    use HasFactory;

    protected $table = 'account_check_operations';
    public $timestamps = false;
    protected $fillable =   [
                                "id",
                                "date",
                                "check_id",
                                "operation",
                                "origin_name",
                                "origin",
                                "partner_id",
                                "notes",
                                "smart_search",
                                "account_check_operation_id",
                                "display_name",
                                "create_uid",
                                "create_date",
                                "write_uid",
                                "write_date",
                                "__last_update"
                            ];

    public function fromDateTime($value){
        return Carbon::parse(parent::fromDateTime($value))->format("Y-d-m H:i:s");
    }                        
}
