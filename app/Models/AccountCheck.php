<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class AccountCheck extends Model
{
    use HasFactory;

    protected $table = 'account_checks';

    public $timestamps = false;

    protected $fillable = [
        'id',
        "operation_ids",
        "name",
        "number",
        "checkbook_id",
        "issue_check_subtype",
        "type",
        "partner_id",
        "first_partner_id",
        "state",
        "issue_date",
        "owner_vat",
        "owner_name",
        "bank_id",
        "amount",
        "amount_company_currency",
        "currency_id",
        "payment_date",
        "journal_id",
        "company_id",
        "company_currency_id",
        "activity_exception_decoration",
        "activity_exception_icon",
        "activity_ids",
        "activity_state",
        "activity_user_id",
        "activity_type_id",
        "activity_date_deadline",
        "activity_summary",
        "message_is_follower",
        "message_follower_ids",
        "message_partner_ids",
        "message_channel_ids",
        "message_ids",
        "message_unread",
        "message_unread_counter",
        "message_needaction",
        "message_needaction_counter",
        "message_has_error",
        "message_has_error_counter",
        "message_main_attachment_id",
        "failed_message_ids",
        "website_message_ids",
        "message_has_sms_error",
        "message_attachment_count",
        "smart_search",
        "account_check_id",
        "display_name",
        "create_uid",
        "create_date",
        "write_uid",
        "write_date",
        "__last_update",
    ];


    public function fromDateTime($value){
        return Carbon::parse(parent::fromDateTime($value))->format("Y-d-m H:i:s");
    } 

}
