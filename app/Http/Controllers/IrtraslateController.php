<?php

namespace App\Http\Controllers;

use App\Models\Irtraslate;
use Illuminate\Http\Request;
use ripcord;

class IrtraslateController extends Controller
{
    //
    public function index()
    {
        $url = env('URL_DATABASE_ODDO');
        $url_auth = $url . '/xmlrpc/2/common';
        $url_exec = $url . '/xmlrpc/2/object';
        $db = env('DATABASE_ODDO');
        $username =  env('USER_ODDO');
        $password = env('PASSWORD_ODDO');
        $common = ripcord::client($url_auth);
        $uid = $common->authenticate($db, $username, $password, array());
        $models = ripcord::client($url_exec);
//        $fields = $models->execute_kw($db, $uid, $password,
//            'ir.translation', 'fields_get',
//            array(), array('attributes' => array('string', 'type')));
//        dd($fields);

        $ids = $models->execute_kw($db, $uid, $password,
            'ir.translation', 'search',
            array(array())
        );

        $collection = collect($ids);

        Irtraslate::truncate();

        foreach($collection->chunk(100) as $idschunks) {
            sleep(10);
            foreach ($idschunks->toArray() as $id) {
                $record = $models->execute_kw($db, $uid, $password,
                    'ir.translation',
                    'read',
                    [$id],
                    array('fields' => array(
                        "name",
                        "res_id",
                        "lang",
                        "type",
                        "src",
                        "value",
                        "module",
                        "state",
                        "comments",
                        "transifex_url",
                        "smart_search",
                        "id",
                        "display_name",
                        "__last_update",
                    )));

                Irtraslate::create([
                    "name" => json_encode(!isset($record[0]["name" ]) ? "name"  : $record[0]["name" ]),
                    "res_id" => json_encode(!isset($record[0]["res_id" ]) ? "res_id"  : $record[0]["res_id" ]),
                    "lang" => json_encode(!isset($record[0]["lang" ]) ? "lang"  : $record[0]["lang" ]),
                    "type" => json_encode(!isset($record[0]["type" ]) ? "type"  : $record[0]["type" ]),
                    "src" => json_encode(!isset($record[0]["src" ]) ? "src"  : $record[0]["src" ]),
                    "value" => json_encode(!isset($record[0]["value" ]) ? "value"  : $record[0]["value" ]),
                    "module" => json_encode(!isset($record[0]["module" ]) ? "module"  : $record[0]["module" ]),
                    "state" => json_encode(!isset($record[0]["state" ]) ? "state"  : $record[0]["state" ]),
                    "comments" => json_encode(!isset($record[0]["comments" ]) ? "comments"  : $record[0]["comments" ]),
                    "transifex_url" => json_encode(!isset($record[0]["transifex_url" ]) ? "transifex_url"  : $record[0]["transifex_url" ]),
                    "smart_search" => json_encode(!isset($record[0]["smart_search" ]) ? "smart_search"  : $record[0]["smart_search" ]),
                    "irtraslate_id" => json_encode(!isset($record[0]["id" ]) ? "id"  : $record[0]["id" ]),
                    "display_name" => json_encode(!isset($record[0]["display_name" ]) ? "display_name"  : $record[0]["display_name" ]),
                    "__last_update" => json_encode(!isset($record[0]["__last_update" ]) ? "__last_update"  : $record[0]["__last_update" ]),
                ]);
            }
        }

        return $this->successResponse("Finalizado carga de datos a la tabla ir.translation", 200);

    }
}
