<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class OdooController extends ApiController
{
    //
    public function index(){
        $odoo = new \Edujugon\Laradoo\Odoo();
        $odoo = $odoo->connect();
        $userId= $odoo->getUid();
        return $this->successResponse($userId,200);
    }
}

