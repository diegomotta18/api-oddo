<?php

namespace App\Http\Controllers;

use App\Models\AccountMove;
use App\Models\ProductSupplierInfo;
use Illuminate\Http\Request;
use ripcord;

class ProductSupplierInfoController extends Controller
{
    //
    public function index()
    {

        $url = env('URL_DATABASE_ODDO');
        $url_auth = $url . '/xmlrpc/2/common';
        $url_exec = $url . '/xmlrpc/2/object';
        $db = env('DATABASE_ODDO');
        $username =  env('USER_ODDO');
        $password = env('PASSWORD_ODDO');
        $common = ripcord::client($url_auth);
        $uid = $common->authenticate($db, $username, $password, array());

        $models = ripcord::client($url_exec);
//
//         $fields = $models->execute_kw($db, $uid, $password,
//         'product.supplierinfo', 'fields_get',
//         array(), array('attributes' => array('string', 'type')));
//         dd($fields);

        $ids = $models->execute_kw(
            $db,
            $uid,
            $password,
            'product.supplierinfo',
            'search',
            array(array())
        );

        $collection = collect($ids);

        ProductSupplierInfo::truncate();

        foreach ($collection->chunk(200) as $key => $idschunks) {
            sleep(10);

            foreach ($idschunks->toArray() as $id) {
                $record = $models->execute_kw(
                    $db,
                    $uid,
                    $password,
                    'product.supplierinfo',
                    'read',
                    array($id),
                    array('fields' => array(
                        "name",
                        "product_name",
                        "product_code",
                        "sequence" ,
                        "product_uom",
                        "min_qty",
                        "price",
                        "company_id",
                        "currency_id",
                        "date_start",
                        "date_end",
                        "product_id",
                        "product_tmpl_id",
                        "product_variant_count",
                        "delay",
                        "last_date_price_updated",
                        "replenishment_cost_rule_id",
                        "net_price",
                        "discount",
                        "smart_search",
                        "id",
                        "display_name",
                        "create_uid",
                        "create_date",
                        "write_uid",
                        "write_date",
                        "__last_update"
                    )));

                ProductSupplierInfo::create([
                    "name"=>json_encode(!isset($record[0]["name"]) ? "name" : $record[0]["name"]),
                    "product_name"=>json_encode(!isset($record[0]["product_name"]) ? "product_name" : $record[0]["product_name"]),
                    "product_code"=>json_encode(!isset($record[0]["product_code"]) ? "product_code" : $record[0]["product_code"]),
                    "sequence" =>json_encode(!isset($record[0]["sequence" ]) ? "sequence"  : $record[0]["sequence" ]),
                    "product_uom"=>json_encode(!isset($record[0]["product_uom"]) ? "product_uom" : $record[0]["product_uom"]),
                    "min_qty"=>json_encode(!isset($record[0]["min_qty"]) ? "min_qty" : $record[0]["min_qty"]),
                    "price"=>json_encode(!isset($record[0]["price"]) ? "price" : $record[0]["price"]),
                    "company_id"=>json_encode(!isset($record[0]["company_id"]) ? "company_id" : $record[0]["company_id"]),
                    "currency_id"=>json_encode(!isset($record[0]["currency_id"]) ? "currency_id" : $record[0]["currency_id"]),
                    "date_start"=>json_encode(!isset($record[0]["date_start"]) ? "date_start" : $record[0]["date_start"]),
                    "date_end"=>json_encode(!isset($record[0]["date_end"]) ? "date_end" : $record[0]["date_end"]),
                    "product_id"=>json_encode(!isset($record[0]["product_id"]) ? "product_id" : $record[0]["product_id"]),
                    "product_tmpl_id"=>json_encode(!isset($record[0]["product_tmpl_id"]) ? "product_tmpl_id" : $record[0]["product_tmpl_id"]),
                    "product_variant_count"=>json_encode(!isset($record[0]["product_variant_count"]) ? "product_variant_count" : $record[0]["product_variant_count"]),
                    "delay"=>json_encode(!isset($record[0]["delay"]) ? "delay" : $record[0]["delay"]),
                    "last_date_price_updated"=>json_encode(!isset($record[0]["last_date_price_updated"]) ? "last_date_price_updated" : $record[0]["last_date_price_updated"]),
                    "replenishment_cost_rule_id"=>json_encode(!isset($record[0]["replenishment_cost_rule_id"]) ? "replenishment_cost_rule_id" : $record[0]["replenishment_cost_rule_id"]),
                    "net_price"=>json_encode(!isset($record[0]["net_price"]) ? "net_price" : $record[0]["net_price"]),
                    "discount"=>json_encode(!isset($record[0]["discount"]) ? "discount" : $record[0]["discount"]),
                    "smart_search"=>json_encode(!isset($record[0]["smart_search"]) ? "smart_search" : $record[0]["smart_search"]),
                    "product_supplier_infos_id"=>json_encode(!isset($record[0]["id"]) ? "id" : $record[0]["id"]),
                    "display_name"=>json_encode(!isset($record[0]["display_name"]) ? "display_name" : $record[0]["display_name"]),
                    "create_uid"=>json_encode(!isset($record[0]["create_uid"]) ? "create_uid" : $record[0]["create_uid"]),
                    "create_date"=>json_encode(!isset($record[0]["create_date"]) ? "create_date" : $record[0]["create_date"]),
                    "write_uid"=>json_encode(!isset($record[0]["write_uid"]) ? "write_uid" : $record[0]["write_uid"]),
                    "write_date"=>json_encode(!isset($record[0]["write_date"]) ? "write_date" : $record[0]["write_date"]),
                    "__last_update"=>json_encode(!isset($record[0]["__last_update"]) ? "__last_update" : $record[0]["__last_update"]),
                ]);
            }
        }

        return $this->successResponse("Finalizado carga de datos a la tabla product_supplier_info", 200);
    }

}
