<?php

namespace App\Http\Controllers;

use App\Models\PurchaseOrder;
use Illuminate\Http\Request;
use ripcord;

class OrdenCompraCabezalController extends ApiController
{
    //

    public function index()
    {

        $url = env('URL_DATABASE_ODDO');
        $url_auth = $url . '/xmlrpc/2/common';
        $url_exec = $url . '/xmlrpc/2/object';
        $db = env('DATABASE_ODDO');
        $username =  env('USER_ODDO');
        $password = env('PASSWORD_ODDO');
        $common = ripcord::client($url_auth);
        $uid = $common->authenticate($db, $username, $password, array());

        $models = ripcord::client($url_exec);

        $ids = $models->execute_kw(
            $db,
            $uid,
            $password,
            'purchase.order',
            'search',
            array(array())
        );
        $collection = collect($ids);

        PurchaseOrder::truncate();

        foreach ($collection->chunk(200) as $key => $idschunks) {
            sleep(10);
            foreach ($idschunks->toArray() as $prod) {
                $record = $models->execute_kw(
                    $db,
                    $uid,
                    $password,
                    'purchase.order',
                    'read',
                    array($prod),

                    array('fields' => array(
                        "name",
                        "origin",
                        "partner_ref",
                        "date_order",
                        "date_approve",
                        "partner_id",
                        "dest_address_id",
                        "currency_id",
                        "state",
                        "order_line",
                        "notes",
                        "invoice_count",
                        "invoice_ids",
                        "invoice_status",
                        "date_planned",
                        "amount_untaxed",
                        "amount_tax",
                        "amount_total",
                        "fiscal_position_id",
                        "payment_term_id",
                        "product_id",
                        "user_id",
                        "company_id",
                        "currency_rate",
                        "incoterm_id",
                        "picking_count",
                        "picking_ids",
                        "picking_type_id",
                        "default_location_dest_id_usage",
                        "group_id",
                        "is_shipped",
                        "internal_notes",
                        "force_invoiced_status",
                        "force_delivered_status",
                        "delivery_status",
                        "with_returns",
                        "activity_exception_decoration",
                        "activity_exception_icon",
                        "activity_ids",
                        "activity_state",
                        "activity_user_id",
                        "activity_type_id",
                        "activity_date_deadline",
                        "activity_summary",
                        "message_is_follower",
                        "message_follower_ids",
                        "message_partner_ids",
                        "message_channel_ids",
                        "message_ids",
                        "message_unread",
                        "message_unread_counter",
                        "message_needaction",
                        "message_needaction_counter",
                        "message_has_error",
                        "message_has_error_counter",
                        "message_main_attachment_id",
                        "failed_message_ids",
                        "website_message_ids",
                        "message_has_sms_error",
                        "message_attachment_count",
                        "smart_search",
                        "access_url",
                        "access_token",
                        "access_warning",
                        "id",
                        "display_name",
                        "create_uid",
                        "create_date",
                        "write_uid",
                        "write_date",
                        "__last_update",
                    ))
                );
                
                PurchaseOrder::create([
                    "name" => json_encode(!isset($record[0]["name"]) ? "name" : $record[0]["name"]),
                    "origin" => json_encode(!isset($record[0]["origin"]) ? "origin" : $record[0]["origin"]),
                    "partner_ref" => json_encode(!isset($record[0]["partner_ref"]) ? "partner_ref" : $record[0]["partner_ref"]),
                    "date_order" => json_encode(!isset($record[0]["date_order"]) ? "date_order" : $record[0]["date_order"]),
                    "date_approve" => json_encode(!isset($record[0]["date_approve"]) ? "date_approve" : $record[0]["date_approve"]),
                    "partner_id" => json_encode(!isset($record[0]["partner_id"]) ? "partner_id" : $record[0]["partner_id"]),
                    "dest_address_id" => json_encode(!isset($record[0]["dest_address_id"]) ? "dest_address_id" : $record[0]["dest_address_id"]),
                    "currency_id" => json_encode(!isset($record[0]["currency_id"]) ? "currency_id" : $record[0]["currency_id"]),
                    "state" => json_encode(!isset($record[0]["state"]) ? "state" : $record[0]["state"]),
                    "order_line" => json_encode(!isset($record[0]["order_line"]) ? "order_line" : $record[0]["order_line"]),
                    "notes" => json_encode(!isset($record[0]["notes"]) ? "notes" : $record[0]["notes"]),
                    "invoice_count" => json_encode(!isset($record[0]["invoice_count"]) ? "invoice_count" : $record[0]["invoice_count"]),
                    "invoice_ids" => json_encode(!isset($record[0]["invoice_ids"]) ? "invoice_ids" : $record[0]["invoice_ids"]),
                    "invoice_status" => json_encode(!isset($record[0]["invoice_status"]) ? "invoice_status" : $record[0]["invoice_status"]),
                    "date_planned" => json_encode(!isset($record[0]["date_planned"]) ? "date_planned" : $record[0]["date_planned"]),
                    "amount_untaxed" => json_encode(!isset($record[0]["amount_untaxed"]) ? "amount_untaxed" : $record[0]["amount_untaxed"]),
                    "amount_tax" => json_encode(!isset($record[0]["amount_tax"]) ? "amount_tax" : $record[0]["amount_tax"]),
                    "amount_total" => json_encode(!isset($record[0]["amount_total"]) ? "amount_total" : $record[0]["amount_total"]),
                    "fiscal_position_id" => json_encode(!isset($record[0]["fiscal_position_id"]) ? "fiscal_position_id" : $record[0]["fiscal_position_id"]),
                    "payment_term_id" => json_encode(!isset($record[0]["payment_term_id"]) ? "payment_term_id" : $record[0]["payment_term_id"]),
                    "product_id" => json_encode(!isset($record[0]["product_id"]) ? "product_id" : $record[0]["product_id"]),
                    "user_id" => json_encode(!isset($record[0]["user_id"]) ? "user_id" : $record[0]["user_id"]),
                    "company_id" => json_encode(!isset($record[0]["company_id"]) ? "company_id" : $record[0]["company_id"]),
                    "currency_rate" => json_encode(!isset($record[0]["currency_rate"]) ? "currency_rate" : $record[0]["currency_rate"]),
                    "incoterm_id" => json_encode(!isset($record[0]["incoterm_id"]) ? "incoterm_id" : $record[0]["incoterm_id"]),
                    "picking_count" => json_encode(!isset($record[0]["picking_count"]) ? "picking_count" : $record[0]["picking_count"]),
                    "picking_ids" => json_encode(!isset($record[0]["picking_ids"]) ? "picking_ids" : $record[0]["picking_ids"]),
                    "picking_type_id" => json_encode(!isset($record[0]["picking_type_id"]) ? "picking_type_id" : $record[0]["picking_type_id"]),
                    "default_location_dest_id_usage" => json_encode(!isset($record[0]["default_location_dest_id_usage"]) ? "default_location_dest_id_usage" : $record[0]["default_location_dest_id_usage"]),
                    "group_id" => json_encode(!isset($record[0]["group_id"]) ? "group_id" : $record[0]["group_id"]),
                    "is_shipped" => json_encode(!isset($record[0]["is_shipped"]) ? "is_shipped" : $record[0]["is_shipped"]),
                    "internal_notes" => json_encode(!isset($record[0]["internal_notes"]) ? "internal_notes" : $record[0]["internal_notes"]),
                    "force_invoiced_status" => json_encode(!isset($record[0]["force_invoiced_status"]) ? "force_invoiced_status" : $record[0]["force_invoiced_status"]),
                    "force_delivered_status" => json_encode(!isset($record[0]["force_delivered_status"]) ? "force_delivered_status" : $record[0]["force_delivered_status"]),
                    "delivery_status" => json_encode(!isset($record[0]["delivery_status"]) ? "delivery_status" : $record[0]["delivery_status"]),
                    "with_returns" => json_encode(!isset($record[0]["with_returns"]) ? "with_returns" : $record[0]["with_returns"]),
                    "activity_exception_decoration" => json_encode(!isset($record[0]["activity_exception_decoration"]) ? "activity_exception_decoration" : $record[0]["activity_exception_decoration"]),
                    "activity_exception_icon" => json_encode(!isset($record[0]["activity_exception_icon"]) ? "activity_exception_icon" : $record[0]["activity_exception_icon"]),
                    "activity_ids" => json_encode(!isset($record[0]["activity_ids"]) ? "activity_ids" : $record[0]["activity_ids"]),
                    "activity_state" => json_encode(!isset($record[0]["activity_state"]) ? "activity_state" : $record[0]["activity_state"]),
                    "activity_user_id" => json_encode(!isset($record[0]["activity_user_id"]) ? "activity_user_id" : $record[0]["activity_user_id"]),
                    "activity_type_id" => json_encode(!isset($record[0]["activity_type_id"]) ? "activity_type_id" : $record[0]["activity_type_id"]),
                    "activity_date_deadline" => json_encode(!isset($record[0]["activity_date_deadline"]) ? "activity_date_deadline" : $record[0]["activity_date_deadline"]),
                    "activity_summary" => json_encode(!isset($record[0]["activity_summary"]) ? "activity_summary" : $record[0]["activity_summary"]),
                    "message_is_follower" => json_encode(!isset($record[0]["message_is_follower"]) ? "message_is_follower" : $record[0]["message_is_follower"]),
                    "message_follower_ids" => json_encode(!isset($record[0]["message_follower_ids"]) ? "message_follower_ids" : $record[0]["message_follower_ids"]),
                    "message_partner_ids" => json_encode(!isset($record[0]["message_partner_ids"]) ? "message_partner_ids" : $record[0]["message_partner_ids"]),
                    "message_channel_ids" => json_encode(!isset($record[0]["message_channel_ids"]) ? "message_channel_ids" : $record[0]["message_channel_ids"]),
                    "message_ids" => json_encode(!isset($record[0]["message_ids"]) ? "message_ids" : $record[0]["message_ids"]),
                    "message_unread" => json_encode(!isset($record[0]["message_unread"]) ? "message_unread" : $record[0]["message_unread"]),
                    "message_unread_counter" => json_encode(!isset($record[0]["message_unread_counter"]) ? "message_unread_counter" : $record[0]["message_unread_counter"]),
                    "message_needaction" => json_encode(!isset($record[0]["message_needaction"]) ? "message_needaction" : $record[0]["message_needaction"]),
                    "message_needaction_counter" => json_encode(!isset($record[0]["message_needaction_counter"]) ? "message_needaction_counter" : $record[0]["message_needaction_counter"]),
                    "message_has_error" => json_encode(!isset($record[0]["message_has_error"]) ? "message_has_error" : $record[0]["message_has_error"]),
                    "message_has_error_counter" => json_encode(!isset($record[0]["message_has_error_counter"]) ? "message_has_error_counter" : $record[0]["message_has_error_counter"]),
                    "message_main_attachment_id" => json_encode(!isset($record[0]["message_main_attachment_id"]) ? "message_main_attachment_id" : $record[0]["message_main_attachment_id"]),
                    "failed_message_ids" => json_encode(!isset($record[0]["failed_message_ids"]) ? "failed_message_ids" : $record[0]["failed_message_ids"]),
                    "website_message_ids" => json_encode(!isset($record[0]["website_message_ids"]) ? "website_message_ids" : $record[0]["website_message_ids"]),
                    "message_has_sms_error" => json_encode(!isset($record[0]["message_has_sms_error"]) ? "message_has_sms_error" : $record[0]["message_has_sms_error"]),
                    "message_attachment_count" => json_encode(!isset($record[0]["message_attachment_count"]) ? "message_attachment_count" : $record[0]["message_attachment_count"]),
                    "smart_search" => json_encode(!isset($record[0]["smart_search"]) ? "smart_search" : $record[0]["smart_search"]),
                    "access_url" => json_encode(!isset($record[0]["access_url"]) ? "access_url" : $record[0]["access_url"]),
                    "access_token" => json_encode(!isset($record[0]["access_token"]) ? "access_token" : $record[0]["access_token"]),
                    "access_warning" => json_encode(!isset($record[0]["access_warning"]) ? "access_warning" : $record[0]["access_warning"]),
                    "purchase_order_id" => json_encode(!isset($record[0]["id"]) ? "id" : $record[0]["id"]),
                    "display_name" => json_encode(!isset($record[0]["display_name"]) ? "display_name" : $record[0]["display_name"]),
                    "create_uid" => json_encode(!isset($record[0]["create_uid"]) ? "create_uid" : $record[0]["create_uid"]),
                    "create_date" => json_encode(!isset($record[0]["create_date"]) ? "create_date" : $record[0]["create_date"]),
                    "write_uid" => json_encode(!isset($record[0]["write_uid"]) ? "write_uid" : $record[0]["write_uid"]),
                    "write_date" => json_encode(!isset($record[0]["write_date"]) ? "write_date" : $record[0]["write_date"]),
                    "__last_update" => json_encode(!isset($record[0]["__last_update"]) ? "__last_update" : $record[0]["__last_update"]),
                ]);
            }
        }

        return $this->successResponse("Finalizado carga de datos a la tabla purchase_orders", 200);
    }
}
