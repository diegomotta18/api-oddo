<?php

namespace App\Http\Controllers;

use App\Models\PurchaseOrderLine;
use Illuminate\Http\Request;
use ripcord;

class OrdenCompraDetalleController extends ApiController
{
    //
    public function index()
    {

        $url = env('URL_DATABASE_ODDO');
        $url_auth = $url . '/xmlrpc/2/common';
        $url_exec = $url . '/xmlrpc/2/object';
        $db = env('DATABASE_ODDO');
        $username =  env('USER_ODDO');
        $password = env('PASSWORD_ODDO');
        $common = ripcord::client($url_auth);
        $uid = $common->authenticate($db, $username, $password, array());

        $models = ripcord::client($url_exec);

        $ids = $models->execute_kw($db, $uid, $password,
            'purchase.order.line', 'search',
            array(array())
        );


        $collection = collect($ids);

        PurchaseOrderLine::truncate();

        foreach ($collection->chunk(200) as $key => $idschunks) {
            sleep(10);
            foreach ($idschunks->toArray() as $id) {
                $record = $models->execute_kw(
                    $db,
                    $uid,
                    $password,
                    'purchase.order.line',
                    'read',
                    [$id],

                    array('fields' => array(
                        "name",
                        "sequence",
                        "product_qty",
                        "product_uom_qty",
                        "date_planned",
                        "taxes_id",
                        "product_uom",
                        "product_uom_category_id",
                        "product_id",
                        "product_type",
                        "price_unit",
                        "price_subtotal",
                        "price_total",
                        "price_tax",
                        "account_analytic_id",
                        "analytic_tag_ids",
                        "company_id",
                        "state",
                        "invoice_lines",
                        "qty_invoiced",
                        "qty_received",
                        "qty_received_manual",
                        "partner_id",
                        "currency_id",
                        "date_order",
                        "display_type",
                        "qty_received_method",
                        "move_ids",
                        "orderpoint_id",
                        "move_dest_ids",
                        "propagate_date",
                        "propagate_date_minimum_delta",
                        "propagate_cancel",
                        "discount",
                        "order_id",
                        "invoice_status",
                        "qty_to_invoice",
                        "sale_order_id",
                        "sale_line_id",
                        "delivery_status",
                        "vouchers",
                        "qty_on_voucher",
                        "qty_returned",
                        "smart_search",
                        "id",
                        "display_name",
                        "create_uid",
                        "create_date",
                        "write_uid",
                        "write_date",
                        "__last_update",
                    ))
                );

                PurchaseOrderLine::create([
                    "name" => json_encode(!isset($record[0]["name"]) ? "name" : $record[0]["name"]),
                    "sequence" =>  json_encode(!isset($record[0]["sequence"]) ? "sequence" : $record[0]["sequence"]),
                    "product_qty" => json_encode(!isset($record[0]["product_qty"]) ? "product_qty" : $record[0]["product_qty"]),
                    "product_uom_qty" => json_encode(!isset($record[0]["product_uom_qty"]) ? "product_uom_qty" : $record[0]["product_uom_qty"]),
                    "date_planned" => json_encode(!isset($record[0]["date_planned"]) ? "date_planned" : $record[0]["date_planned"]),
                    "taxes_id" => json_encode(!isset($record[0]["taxes_id"]) ? "taxes_id" : $record[0]["taxes_id"]),
                    "product_uom" => json_encode(!isset($record[0]["product_uom"]) ? "product_uom" : $record[0]["product_uom"]),
                    "product_uom_category_id" => json_encode(!isset($record[0]["product_uom_category_id"]) ? "product_uom_category_id" : $record[0]["product_uom_category_id"]),
                    "product_id" => json_encode(!isset($record[0]["product_id"]) ? "product_id" : $record[0]["product_id"]),
                    "product_type" => json_encode(!isset($record[0]["product_type"]) ? "product_type" : $record[0]["product_type"]),
                    "price_unit" => json_encode(!isset($record[0]["price_unit"]) ? "price_unit" : $record[0]["price_unit"]),
                    "price_subtotal" => json_encode(!isset($record[0]["price_subtotal"]) ? "price_subtotal" : $record[0]["price_subtotal"]),
                    "price_total" => json_encode(!isset($record[0]["price_total"]) ? "price_total" : $record[0]["price_total"]),
                    "price_tax" => json_encode(!isset($record[0]["price_tax"]) ? "price_tax" : $record[0]["price_tax"]),
                    "account_analytic_id" => json_encode(!isset($record[0]["account_analytic_id"]) ? "account_analytic_id" : $record[0]["account_analytic_id"]),
                    "analytic_tag_ids" => json_encode(!isset($record[0]["analytic_tag_ids"]) ? "analytic_tag_ids" : $record[0]["analytic_tag_ids"]),
                    "company_id" => json_encode(!isset($record[0]["company_id"]) ? "company_id" : $record[0]["company_id"]),
                    "state" => json_encode(!isset($record[0]["state"]) ? "state" : $record[0]["state"]),
                    "invoice_lines" => json_encode(!isset($record[0]["invoice_lines"]) ? "invoice_lines" : $record[0]["invoice_lines"]),
                    "qty_invoiced" => json_encode(!isset($record[0]["qty_invoiced"]) ? "qty_invoiced" : $record[0]["qty_invoiced"]),
                    "qty_received" => json_encode(!isset($record[0]["qty_received"]) ? "qty_received" : $record[0]["qty_received"]),
                    "qty_received_manual" => json_encode(!isset($record[0]["qty_received_manual"]) ? "qty_received_manual" : $record[0]["qty_received_manual"]),
                    "partner_id" => json_encode(!isset($record[0]["partner_id"]) ? "partner_id" : $record[0]["partner_id"]),
                    "currency_id" => json_encode(!isset($record[0]["currency_id"]) ? "currency_id" : $record[0]["currency_id"]),
                    "date_order" => json_encode(!isset($record[0]["date_order"]) ? "date_order" : $record[0]["date_order"]),
                    "display_type" => json_encode(!isset($record[0]["display_type"]) ? "display_type" : $record[0]["display_type"]),
                    "qty_received_method" => json_encode(!isset($record[0]["qty_received_method"]) ? "qty_received_method" : $record[0]["qty_received_method"]),
                    "move_ids" => json_encode(!isset($record[0]["move_ids"]) ? "move_ids" : $record[0]["move_ids"]),
                    "orderpoint_id" => json_encode(!isset($record[0]["orderpoint_id"]) ? "orderpoint_id" : $record[0]["orderpoint_id"]),
                    "move_dest_ids" => json_encode(!isset($record[0]["move_dest_ids"]) ? "move_dest_ids" : $record[0]["move_dest_ids"]),
                    "propagate_date" => json_encode(!isset($record[0]["propagate_date"]) ? "propagate_date" : $record[0]["propagate_date"]),
                    "propagate_date_minimum_delta" => json_encode(!isset($record[0]["propagate_date_minimum_delta"]) ? "propagate_date_minimum_delta" : $record[0]["propagate_date_minimum_delta"]),
                    "propagate_cancel" => json_encode(!isset($record[0]["propagate_cancel"]) ? "propagate_cancel" : $record[0]["propagate_cancel"]),
                    "discount" => json_encode(!isset($record[0]["discount"]) ? "discount" : $record[0]["discount"]),
                    "order_id" => json_encode(!isset($record[0]["order_id"]) ? "order_id" : $record[0]["order_id"]),
                    "invoice_status" => json_encode(!isset($record[0]["invoice_status"]) ? "invoice_status" : $record[0]["invoice_status"]),
                    "qty_to_invoice" => json_encode(!isset($record[0]["qty_to_invoice"]) ? "qty_to_invoice" : $record[0]["qty_to_invoice"]),
                    "sale_order_id" => json_encode(!isset($record[0]["sale_order_id"]) ? "sale_order_id" : $record[0]["sale_order_id"]),
                    "sale_line_id" => json_encode(!isset($record[0]["sale_line_id"]) ? "sale_line_id" : $record[0]["sale_line_id"]),
                    "delivery_status" => json_encode(!isset($record[0]["delivery_status"]) ? "delivery_status" : $record[0]["delivery_status"]),
                    "vouchers" => json_encode(!isset($record[0]["vouchers"]) ? "vouchers" : $record[0]["vouchers"]),
                    "qty_on_voucher" => json_encode(!isset($record[0]["qty_on_voucher"]) ? "qty_on_voucher" : $record[0]["qty_on_voucher"]),
                    "qty_returned" => json_encode(!isset($record[0]["qty_returned"]) ? "qty_returned" : $record[0]["qty_returned"]),
                    "smart_search" => json_encode(!isset($record[0]["smart_search"]) ? "smart_search" : $record[0]["smart_search"]),
                    "purchase_order_line_id" => json_encode(!isset($record[0]["id"]) ? "id" : $record[0]["id"]),
                    "display_name" => json_encode(!isset($record[0]["display_name"]) ? "display_name" : $record[0]["display_name"]),
                    "create_uid" => json_encode(!isset($record[0]["create_uid"]) ? "create_uid" : $record[0]["create_uid"]),
                    "create_date" => json_encode(!isset($record[0]["create_date"]) ? "create_date" : $record[0]["create_date"]),
                    "write_uid" => json_encode(!isset($record[0]["write_uid"]) ? "write_uid" : $record[0]["write_uid"]),
                    "write_date" => json_encode(!isset($record[0]["write_date"]) ? "write_date" : $record[0]["write_date"]),
                    "__last_update" => json_encode(!isset($record[0]["__last_update"]) ? "__last_update" : $record[0]["__last_update"]),
                ]);
            }
        }

        return $this->successResponse("Finalizado carga de datos a la tabla purchase_order_lines", 200);
    }
}
