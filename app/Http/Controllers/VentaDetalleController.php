<?php

namespace App\Http\Controllers;

use App\Models\SaleOrderLine;
use Illuminate\Http\Request;
use ripcord;

class VentaDetalleController extends ApiController
{
    public function index()
    {

        $url = env('URL_DATABASE_ODDO');
        $url_auth = $url . '/xmlrpc/2/common';
        $url_exec = $url . '/xmlrpc/2/object';
        //$db         = 'train-quimicamineral-07-07-4';
        $db = env('DATABASE_ODDO');
        $username =  env('USER_ODDO');
        $password = env('PASSWORD_ODDO');
        $common = ripcord::client($url_auth);
        $uid = $common->authenticate($db, $username, $password, array());

        $models = ripcord::client($url_exec);

        $ids = $models->execute_kw(
            $db,
            $uid,
            $password,
            'sale.order.line',
            'search',
            array(array())
        );
        $collection = collect($ids);

        SaleOrderLine::truncate();

        foreach ($collection->chunk(200) as $key => $idschunks) {
            foreach ($idschunks->toArray() as $prod) {
                $record = $models->execute_kw(
                    $db,
                    $uid,
                    $password,
                    'sale.order.line',
                    'read',
                    array($prod),
                    array('fields' => array(
                        "order_id",
                        "name",
                        "sequence",
                        "invoice_lines",
                        "invoice_status",
                        "price_unit",
                        "price_subtotal",
                        "price_tax",
                        "price_total",
                        "price_reduce",
                        "tax_id",
                        "price_reduce_taxinc",
                        "price_reduce_taxexcl",
                        "discount",
                        "product_id",
                        "product_template_id",
                        "product_updatable",
                        "product_uom_qty",
                        "product_uom",
                        "product_uom_category_id",
                        "product_custom_attribute_value_ids",
                        "product_no_variant_attribute_value_ids",
                        "qty_delivered",
                        "qty_delivered_manual",
                        "qty_to_invoice",
                        "qty_invoiced",
                        "untaxed_amount_invoiced",
                        "untaxed_amount_to_invoice",
                        "salesman_id",
                        "currency_id",
                        "company_id",
                        "order_partner_id",
                        "analytic_tag_ids",
                        "is_expense",
                        "is_downpayment",
                        "state",
                        "customer_lead",
                        "display_type",
                        "product_can_modify_prices",
                        "exception_ids",
                        "exceptions_summary",
                        "ignore_exception",
                        "sale_order_option_ids",
                        "is_configurable_product",
                        "product_template_attribute_value_ids",
                        "purchase_line_ids",
                        "purchase_line_count",
                        "product_packaging",
                        "route_id",
                        "move_ids",
                        "product_type",
                        "virtual_available_at_date",
                        "scheduled_date",
                        "free_qty_today",
                        "qty_available_today",
                        "warehouse_id",
                        "qty_to_deliver",
                        "is_mto",
                        "display_qty_widget",
                        "is_delivery",
                        "product_qty",
                        "recompute_delivery_price",
                        "report_price_unit",
                        "price_unit_with_tax",
                        "report_price_subtotal",
                        "report_price_net",
                        "report_tax_id",
                        "vat_tax_id",
                        "report_price_reduce",
                        "subscription_id",
                        "qty_delivered_method",
                        "project_id",
                        "task_id",
                        "is_service",
                        "analytic_line_ids",
                        "all_qty_delivered",
                        "quantity_returned",
                        "delivery_status",
                        "smart_search",
                        "id",
                        "display_name",
                        "create_uid",
                        "create_date",
                        "write_uid",
                        "write_date",
                        "__last_update",
                    ))
                );

                SaleOrderLine::create([
                    "order_id" => json_encode(!isset($record[0]["order_id"]) ? "order_id" : $record[0]["order_id"]),
                    "name" => json_encode(!isset($record[0]["name"]) ? "name" : $record[0]["name"]),
                    "sequence" => json_encode(!isset($record[0]["sequence"]) ? "sequence" : $record[0]["sequence"]),
                    "invoice_lines" => json_encode(!isset($record[0]["invoice_lines"]) ? "invoice_lines" : $record[0]["invoice_lines"]),
                    "invoice_status" => json_encode(!isset($record[0]["invoice_status"]) ? "invoice_status" : $record[0]["invoice_status"]),
                    "price_unit" => json_encode(!isset($record[0]["price_unit"]) ? "price_unit" : $record[0]["price_unit"]),
                    "price_subtotal" => json_encode(!isset($record[0]["price_subtotal"]) ? "price_subtotal" : $record[0]["price_subtotal"]),
                    "price_tax" => json_encode(!isset($record[0]["price_tax"]) ? "price_tax" : $record[0]["price_tax"]),
                    "price_total" => json_encode(!isset($record[0]["price_total"]) ? "price_total" : $record[0]["price_total"]),
                    "price_reduce" => json_encode(!isset($record[0]["price_reduce"]) ? "price_reduce" : $record[0]["price_reduce"]),
                    "tax_id" => json_encode(!isset($record[0]["tax_id"]) ? "tax_id" : $record[0]["tax_id"]),
                    "price_reduce_taxinc" => json_encode(!isset($record[0]["price_reduce_taxinc"]) ? "price_reduce_taxinc" : $record[0]["price_reduce_taxinc"]),
                    "price_reduce_taxexcl" => json_encode(!isset($record[0]["price_reduce_taxexcl"]) ? "price_reduce_taxexcl" : $record[0]["price_reduce_taxexcl"]),
                    "discount" => json_encode(!isset($record[0]["discount"]) ? "discount" : $record[0]["discount"]),
                    "product_id" => json_encode(!isset($record[0]["product_id"]) ? "product_id" : $record[0]["product_id"]),
                    "product_template_id" => json_encode(!isset($record[0]["product_template_id"]) ? "product_template_id" : $record[0]["product_template_id"]),
                    "product_updatable" => json_encode(!isset($record[0]["product_updatable"]) ? "product_updatable" : $record[0]["product_updatable"]),
                    "product_uom_qty" => json_encode(!isset($record[0]["product_uom_qty"]) ? "product_uom_qty" : $record[0]["product_uom_qty"]),
                    "product_uom" => json_encode(!isset($record[0]["product_uom"]) ? "product_uom" : $record[0]["product_uom"]),
                    "product_uom_category_id" => json_encode(!isset($record[0]["product_uom_category_id"]) ? "product_uom_category_id" : $record[0]["product_uom_category_id"]),
                    "product_custom_attribute_value_ids" => json_encode(!isset($record[0]["product_custom_attribute_value_ids"]) ? "product_custom_attribute_value_ids" : $record[0]["product_custom_attribute_value_ids"]),
                    "product_no_variant_attribute_value_ids" => json_encode(!isset($record[0]["product_no_variant_attribute_value_ids"]) ? "product_no_variant_attribute_value_ids" : $record[0]["product_no_variant_attribute_value_ids"]),
                    "qty_delivered" => json_encode(!isset($record[0]["qty_delivered"]) ? "qty_delivered" : $record[0]["qty_delivered"]),
                    "qty_delivered_manual" => json_encode(!isset($record[0]["qty_delivered_manual"]) ? "qty_delivered_manual" : $record[0]["qty_delivered_manual"]),
                    "qty_to_invoice" => json_encode(!isset($record[0]["qty_to_invoice"]) ? "qty_to_invoice" : $record[0]["qty_to_invoice"]),
                    "qty_invoiced" => json_encode(!isset($record[0]["qty_invoiced"]) ? "qty_invoiced" : $record[0]["qty_invoiced"]),
                    "untaxed_amount_invoiced" => json_encode(!isset($record[0]["untaxed_amount_invoiced"]) ? "untaxed_amount_invoiced" : $record[0]["untaxed_amount_invoiced"]),
                    "untaxed_amount_to_invoice" => json_encode(!isset($record[0]["untaxed_amount_to_invoice"]) ? "untaxed_amount_to_invoice" : $record[0]["untaxed_amount_to_invoice"]),
                    "salesman_id" => json_encode(!isset($record[0]["salesman_id"]) ? "salesman_id" : $record[0]["salesman_id"]),
                    "currency_id" => json_encode(!isset($record[0]["currency_id"]) ? "currency_id" : $record[0]["currency_id"]),
                    "company_id" => json_encode(!isset($record[0]["company_id"]) ? "company_id" : $record[0]["company_id"]),
                    "order_partner_id" => json_encode(!isset($record[0]["order_partner_id"]) ? "order_partner_id" : $record[0]["order_partner_id"]),
                    "analytic_tag_ids" => json_encode(!isset($record[0]["analytic_tag_ids"]) ? "analytic_tag_ids" : $record[0]["analytic_tag_ids"]),
                    "is_expense" => json_encode(!isset($record[0]["is_expense"]) ? "is_expense" : $record[0]["is_expense"]),
                    "is_downpayment" => json_encode(!isset($record[0]["is_downpayment"]) ? "is_downpayment" : $record[0]["is_downpayment"]),
                    "state" => json_encode(!isset($record[0]["state"]) ? "state" : $record[0]["state"]),
                    "customer_lead" => json_encode(!isset($record[0]["customer_lead"]) ? "customer_lead" : $record[0]["customer_lead"]),
                    "display_type" => json_encode(!isset($record[0]["display_type"]) ? "display_type" : $record[0]["display_type"]),
                    "product_can_modify_prices" => json_encode(!isset($record[0]["product_can_modify_prices"]) ? "product_can_modify_prices" : $record[0]["product_can_modify_prices"]),
                    "exception_ids" => json_encode(!isset($record[0]["exception_ids"]) ? "exception_ids" : $record[0]["exception_ids"]),
                    "exceptions_summary" => json_encode(!isset($record[0]["exceptions_summary"]) ? "exceptions_summary" : $record[0]["exceptions_summary"]),
                    "ignore_exception" => json_encode(!isset($record[0]["ignore_exception"]) ? "ignore_exception" : $record[0]["ignore_exception"]),
                    "sale_order_option_ids" => json_encode(!isset($record[0]["sale_order_option_ids"]) ? "sale_order_option_ids" : $record[0]["sale_order_option_ids"]),
                    "is_configurable_product" => json_encode(!isset($record[0]["is_configurable_product"]) ? "is_configurable_product" : $record[0]["is_configurable_product"]),
                    "product_template_attribute_value_ids" => json_encode(!isset($record[0]["product_template_attribute_value_ids"]) ? "product_template_attribute_value_ids" : $record[0]["product_template_attribute_value_ids"]),
                    "purchase_line_ids" => json_encode(!isset($record[0]["purchase_line_ids"]) ? "purchase_line_ids" : $record[0]["purchase_line_ids"]),
                    "purchase_line_count" => json_encode(!isset($record[0]["purchase_line_count"]) ? "purchase_line_count" : $record[0]["purchase_line_count"]),
                    "product_packaging" => json_encode(!isset($record[0]["product_packaging"]) ? "product_packaging" : $record[0]["product_packaging"]),
                    "route_id" => json_encode(!isset($record[0]["route_id"]) ? "route_id" : $record[0]["route_id"]),
                    "move_ids" => json_encode(!isset($record[0]["move_ids"]) ? "move_ids" : $record[0]["move_ids"]),
                    "product_type" => json_encode(!isset($record[0]["product_type"]) ? "product_type" : $record[0]["product_type"]),
                    "virtual_available_at_date" => json_encode(!isset($record[0]["virtual_available_at_date"]) ? "virtual_available_at_date" : $record[0]["virtual_available_at_date"]),
                    "scheduled_date" => json_encode(!isset($record[0]["scheduled_date"]) ? "scheduled_date" : $record[0]["scheduled_date"]),
                    "free_qty_today" => json_encode(!isset($record[0]["free_qty_today"]) ? "free_qty_today" : $record[0]["free_qty_today"]),
                    "qty_available_today" => json_encode(!isset($record[0]["qty_available_today"]) ? "qty_available_today" : $record[0]["qty_available_today"]),
                    "warehouse_id" => json_encode(!isset($record[0]["warehouse_id"]) ? "warehouse_id" : $record[0]["warehouse_id"]),
                    "qty_to_deliver" => json_encode(!isset($record[0]["qty_to_deliver"]) ? "qty_to_deliver" : $record[0]["qty_to_deliver"]),
                    "is_mto" => json_encode(!isset($record[0]["is_mto"]) ? "is_mto" : $record[0]["is_mto"]),
                    "display_qty_widget" => json_encode(!isset($record[0]["display_qty_widget"]) ? "display_qty_widget" : $record[0]["display_qty_widget"]),
                    "is_delivery" => json_encode(!isset($record[0]["is_delivery"]) ? "is_delivery" : $record[0]["is_delivery"]),
                    "product_qty" => json_encode(!isset($record[0]["product_qty"]) ? "product_qty" : $record[0]["product_qty"]),
                    "recompute_delivery_price" => json_encode(!isset($record[0]["recompute_delivery_price"]) ? "recompute_delivery_price" : $record[0]["recompute_delivery_price"]),
                    "report_price_unit" => json_encode(!isset($record[0]["report_price_unit"]) ? "report_price_unit" : $record[0]["report_price_unit"]),
                    "price_unit_with_tax" => json_encode(!isset($record[0]["price_unit_with_tax"]) ? "price_unit_with_tax" : $record[0]["price_unit_with_tax"]),
                    "report_price_subtotal" => json_encode(!isset($record[0]["report_price_subtotal"]) ? "report_price_subtotal" : $record[0]["report_price_subtotal"]),
                    "report_price_net" => json_encode(!isset($record[0]["report_price_net"]) ? "report_price_net" : $record[0]["report_price_net"]),
                    "report_tax_id" => json_encode(!isset($record[0]["report_tax_id"]) ? "report_tax_id" : $record[0]["report_tax_id"]),
                    "vat_tax_id" => json_encode(!isset($record[0]["vat_tax_id"]) ? "vat_tax_id" : $record[0]["vat_tax_id"]),
                    "report_price_reduce" => json_encode(!isset($record[0]["report_price_reduce"]) ? "report_price_reduce" : $record[0]["report_price_reduce"]),
                    "subscription_id" => json_encode(!isset($record[0]["subscription_id"]) ? "subscription_id" : $record[0]["subscription_id"]),
                    "qty_delivered_method" => json_encode(!isset($record[0]["qty_delivered_method"]) ? "qty_delivered_method" : $record[0]["qty_delivered_method"]),
                    "project_id" => json_encode(!isset($record[0]["project_id"]) ? "project_id" : $record[0]["project_id"]),
                    "task_id" => json_encode(!isset($record[0]["task_id"]) ? "task_id" : $record[0]["task_id"]),
                    "is_service" => json_encode(!isset($record[0]["is_service"]) ? "is_service" : $record[0]["is_service"]),
                    "analytic_line_ids" => json_encode(!isset($record[0]["analytic_line_ids"]) ? "analytic_line_ids" : $record[0]["analytic_line_ids"]),
                    "all_qty_delivered" => json_encode(!isset($record[0]["all_qty_delivered"]) ? "all_qty_delivered" : $record[0]["all_qty_delivered"]),
                    "quantity_returned" => json_encode(!isset($record[0]["quantity_returned"]) ? "quantity_returned" : $record[0]["quantity_returned"]),
                    "delivery_status" => json_encode(!isset($record[0]["delivery_status"]) ? "delivery_status" : $record[0]["delivery_status"]),
                    "smart_search" => json_encode(!isset($record[0]["smart_search"]) ? "smart_search" : $record[0]["smart_search"]),
                    "sale_order_line_id" => json_encode(!isset($record[0]["id"]) ? "id" : $record[0]["id"]),
                    "display_name" => json_encode(!isset($record[0]["display_name"]) ? "display_name" : $record[0]["display_name"]),
                    "create_uid" => json_encode(!isset($record[0]["create_uid"]) ? "create_uid" : $record[0]["create_uid"]),
                    "create_date" => json_encode(!isset($record[0]["create_date"]) ? "create_date" : $record[0]["create_date"]),
                    "write_uid" => json_encode(!isset($record[0]["write_uid"]) ? "write_uid" : $record[0]["write_uid"]),
                    "write_date" => json_encode(!isset($record[0]["write_date"]) ? "write_date" : $record[0]["write_date"]),
                    "__last_update" => json_encode(!isset($record[0]["__last_update"]) ? "__last_update" : $record[0]["__last_update"]),
                ]);
            }
        }

        return $this->successResponse("Finalizado carga de datos a la tabla sale_order_lines", 200);
    }
}
