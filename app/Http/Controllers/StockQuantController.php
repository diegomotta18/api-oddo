<?php

namespace App\Http\Controllers;

use App\Models\ProductSupplierInfo;
use App\Models\StockQuant;
use Illuminate\Http\Request;
use ripcord;

class StockQuantController extends Controller
{
    //
    public function index(){

        $url = env('URL_DATABASE_ODDO');
        $url_auth = $url . '/xmlrpc/2/common';
        $url_exec = $url . '/xmlrpc/2/object';
        $db = env('DATABASE_ODDO');
        $username =  env('USER_ODDO');
        $password = env('PASSWORD_ODDO');
        $common = ripcord::client($url_auth);
        $uid = $common->authenticate($db, $username, $password, array());

        $models = ripcord::client($url_exec);
//
//         $fields = $models->execute_kw($db, $uid, $password,
//         'product.supplierinfo', 'fields_get',
//         array(), array('attributes' => array('string', 'type')));
//         dd($fields);

        $ids = $models->execute_kw(
            $db,
            $uid,
            $password,
            'stock.quant',
            'search',
            array(array())
        );

        $collection = collect($ids);

        StockQuant::truncate();

        foreach ($collection->chunk(200) as $key => $idschunks) {
            sleep(10);

            foreach ($idschunks->toArray() as $id) {
                $record = $models->execute_kw(
                    $db,
                    $uid,
                    $password,
                    'stock.quant',
                    'read',
                    array($id),
                    array('fields' => array(
                        "product_id",
                        "product_tmpl_id",
                        "product_uom_id",
                        "company_id",
                        "location_id",
                        "lot_id",
                        "package_id",
                        "owner_id",
                        "quantity",
                        "reserved_quantity",
                        "in_date",
                        "tracking",
                        "on_hand",
                        "removal_date",
                        "smart_search",
                        "id",
                        "display_name",
                        "create_uid",
                        "create_date",
                        "write_uid",
                        "write_date",
                        "__last_update",
                    )));

                StockQuant::create([
                    "product_id"=>json_encode(!isset($record[0]["product_id"]) ? "product_id" : $record[0]["product_id"]),
                    "product_tmpl_id"=>json_encode(!isset($record[0]["product_tmpl_id"]) ? "product_tmpl_id" : $record[0]["product_tmpl_id"]),
                    "product_uom_id"=>json_encode(!isset($record[0]["product_uom_id"]) ? "product_uom_id" : $record[0]["product_uom_id"]),
                    "company_id"=>json_encode(!isset($record[0]["company_id"]) ? "company_id" : $record[0]["company_id"]),
                    "location_id"=>json_encode(!isset($record[0]["location_id"]) ? "location_id" : $record[0]["location_id"]),
                    "lot_id"=>json_encode(!isset($record[0]["lot_id"]) ? "lot_id" : $record[0]["lot_id"]),
                    "package_id"=>json_encode(!isset($record[0]["package_id"]) ? "package_id" : $record[0]["package_id"]),
                    "owner_id"=>json_encode(!isset($record[0]["owner_id"]) ? "owner_id" : $record[0]["owner_id"]),
                    "quantity"=>json_encode(!isset($record[0]["quantity"]) ? "quantity" : $record[0]["quantity"]),
                    "reserved_quantity"=>json_encode(!isset($record[0]["reserved_quantity"]) ? "reserved_quantity" : $record[0]["reserved_quantity"]),
                    "in_date"=>json_encode(!isset($record[0]["in_date"]) ? "in_date" : $record[0]["in_date"]),
                    "tracking"=>json_encode(!isset($record[0]["tracking"]) ? "tracking" : $record[0]["tracking"]),
                    "on_hand"=>json_encode(!isset($record[0]["on_hand"]) ? "on_hand" : $record[0]["on_hand"]),
                    "removal_date"=>json_encode(!isset($record[0]["removal_date"]) ? "removal_date" : $record[0]["removal_date"]),
                    "smart_search"=>json_encode(!isset($record[0]["smart_search"]) ? "smart_search" : $record[0]["smart_search"]),
                    "stock_quant_id"=>json_encode(!isset($record[0]["id"]) ? "id" : $record[0]["id"]),
                    "display_name"=>json_encode(!isset($record[0]["display_name"]) ? "display_name" : $record[0]["display_name"]),
                    "create_uid"=>json_encode(!isset($record[0]["create_uid"]) ? "create_uid" : $record[0]["create_uid"]),
                    "create_date"=>json_encode(!isset($record[0]["create_date"]) ? "create_date" : $record[0]["create_date"]),
                    "write_uid"=>json_encode(!isset($record[0]["write_uid"]) ? "write_uid" : $record[0]["write_uid"]),
                    "write_date"=>json_encode(!isset($record[0]["write_date"]) ? "write_date" : $record[0]["write_date"]),
                    "__last_update"=>json_encode(!isset($record[0]["__last_update"]) ? "__last_update" : $record[0]["__last_update"]),
                ]);
            }
        }

        return $this->successResponse("Finalizado carga de datos a la tabla stock_quant", 200);

    }
}
