<?php

namespace App\Http\Controllers;

use App\Models\AccountMove;
use Illuminate\Http\Request;
use ripcord;

class FacturaCabezalClienteController extends ApiController
{
    //
    public function index()
    {

        $url = env('URL_DATABASE_ODDO');
        $url_auth = $url . '/xmlrpc/2/common';
        $url_exec = $url . '/xmlrpc/2/object';
        $db = env('DATABASE_ODDO');
        $username =  env('USER_ODDO');
        $password = env('PASSWORD_ODDO');
        $common = ripcord::client($url_auth);
        $uid = $common->authenticate($db, $username, $password, array());

        $models = ripcord::client($url_exec);

        // $fields = $models->execute_kw($db, $uid, $password,
        // 'account.move', 'fields_get',
        // array(), array('attributes' => array('string', 'type')));
        // dd($fields);

        $ids = $models->execute_kw(
            $db,
            $uid,
            $password,
            'account.move',
            'search',
            array(array())
        );

        $collection = collect($ids);

        AccountMove::truncate();

        foreach ($collection->chunk(200) as $key => $idschunks) {
            sleep(10);

            foreach ($idschunks->toArray() as $id) {
                $record = $models->execute_kw(
                    $db,
                    $uid,
                    $password,
                    'account.move',
                    'read',
                    array($id),
                    array('fields' => array(
                        "name",
                        "date",
                        "ref",
                        "narration",
                        "state",
                        "type",
                        "type_name",
                        "to_check",
                        "company_id",
                        "company_currency_id",
                        "currency_id",
                        "line_ids",
                        "commercial_partner_id",
                        "amount_untaxed",
                        "amount_tax",
                        "amount_total",
                        "amount_residual",
                        "amount_untaxed_signed",
                        "amount_tax_signed",
                        "amount_total_signed",
                        "amount_residual_signed",
                        "amount_by_group",
                        "tax_cash_basis_rec_id",
                        "auto_post",
                        "invoice_user_id",
                        "user_id",
                        "invoice_payment_state",
                        "invoice_date",
                        "invoice_date_due",
                        "invoice_payment_ref",
                        "invoice_sent",
                        "invoice_origin",
                        "invoice_line_ids",
                        "invoice_incoterm_id",
                        "invoice_outstanding_credits_debits_widget",
                        "invoice_payments_widget",
                        "invoice_has_outstanding",
                        "invoice_source_email",
                        "invoice_partner_display_name",
                        "invoice_partner_icon",
                        "invoice_cash_rounding_id",
                        "invoice_sequence_number_next",
                        "invoice_sequence_number_next_prefix",
                        "invoice_filter_type_domain",
                        "bank_partner_id",
                        "invoice_has_matching_suspense_amount",
                        "tax_lock_date_message",
                        "has_reconciled_entries",
                        "restrict_mode_hash_table",
                        "secure_sequence_number",
                        "inalterable_hash",
                        "string_to_hash",
                        "attachment_ids",
                        "debit_origin_id",
                        "debit_note_ids",
                        "debit_note_count",
                        "duplicated_vendor_ref",
                        "extract_state",
                        "extract_status_code",
                        "extract_error_message",
                        "extract_remote_id",
                        "extract_word_ids",
                        "extract_can_show_resend_button",
                        "extract_can_show_send_button",
                        "journal_id",
                        "partner_id",
                        "fiscal_position_id",
                        "invoice_payment_term_id",
                        "invoice_partner_bank_id",
                        "invoice_vendor_bill_id",
                        "l10n_latam_amount_untaxed",
                        "l10n_latam_tax_ids",
                        "l10n_latam_available_document_type_ids",
                        "l10n_latam_document_type_id",
                        "l10n_latam_sequence_id",
                        "l10n_latam_document_number",
                        "l10n_latam_use_documents",
                        "l10n_latam_country_code",
                        "transaction_ids",
                        "authorized_transaction_ids",
                        "purchase_vendor_bill_id",
                        "purchase_id",
                        "stock_move_id",
                        "stock_valuation_layer_ids",
                        "transfer_model_id",
                        "is_tax_closing",
                        "tax_report_control_error",
                        "internal_notes",
                        "reversed_entry_id",
                        "team_id",
                        "partner_shipping_id",
                        "asset_id",
                        "asset_asset_type",
                        "asset_remaining_value",
                        "asset_depreciated_value",
                        "asset_manually_modified",
                        "asset_value_change",
                        "asset_ids",
                        "asset_ids_display_name",
                        "asset_id_display_name",
                        "number_asset_ids",
                        "draft_asset_ids",
                        "reversal_move_id",
                        "move_currency_id",
                        "move_inverse_currency_rate",
                        "open_move_line_ids",
                        "pay_now_journal_id",
                        "payment_group_ids",
                        "l10n_ar_afip_responsibility_type_id",
                        "l10n_ar_currency_rate",
                        "l10n_ar_afip_concept",
                        "l10n_ar_afip_service_start",
                        "l10n_ar_afip_service_end",
                        "purchase_order_ids",
                        "has_purchases",
                        "sale_order_ids",
                        "has_sales",
                        "rejected_check_id",
                        "exchange_diff_adjustment_required",
                        "exchange_diff_ignored",
                        "exchange_diff_invoice_ids",
                        "settled_line_ids",
                        "l10n_ar_afip_auth_mode",
                        "l10n_ar_afip_auth_code",
                        "l10n_ar_afip_auth_code_due",
                        "l10n_ar_afip_barcode",
                        "l10n_ar_afip_qr_code",
                        "l10n_ar_afip_result",
                        "l10n_ar_afip_ws",
                        "l10n_ar_afip_verification_type",
                        "l10n_ar_afip_verification_result",
                        "l10n_ar_afip_fce_is_cancellation",
                        "sale_type_id",
                        "purchase_order_number",
                        "timesheet_ids",
                        "timesheet_count",
                        "picking_ids",
                        "computed_currency_rate",
                        "l10n_ar_afip_asoc_period_start",
                        "l10n_ar_afip_asoc_period_end",
                        "display_currency_alert",
                        "document_type_internal_type",
                        "invoice_validation_type",
                        "campaign_id",
                        "source_id",
                        "medium_id",
                        "activity_exception_decoration",
                        "activity_exception_icon",
                        "activity_ids",
                        "activity_state",
                        "activity_user_id",
                        "activity_type_id",
                        "activity_date_deadline",
                        "activity_summary",
                        "message_is_follower",
                        "message_follower_ids",
                        "message_partner_ids",
                        "message_channel_ids",
                        "message_ids",
                        "message_unread",
                        "message_unread_counter",
                        "message_needaction",
                        "message_needaction_counter",
                        "message_has_error",
                        "message_has_error_counter",
                        "message_main_attachment_id",
                        "failed_message_ids",
                        "website_message_ids",
                        "message_has_sms_error",
                        "message_attachment_count",
                        "access_url",
                        "access_token",
                        "access_warning",
                        "smart_search",
                        "id",
                        "display_name",
                        "create_uid",
                        "create_date",
                        "write_uid",
                        "write_date",
                        "__last_update",
                    )));

                    AccountMove::create([
                    "name" =>json_encode(!isset($record[0]["name"]) ? "name" : $record[0]["name"]),
                    "date" => json_encode(!isset($record[0]["date"]) ? "date" : $record[0]["date"]),
                    "ref" => json_encode(!isset($record[0]["ref"]) ? "ref" : $record[0]["ref"]),
                    "narration" => json_encode(!isset($record[0]["narration"]) ? "narration" : $record[0]["narration"]),
                    "state" => json_encode(!isset($record[0]["state"]) ? "state" : $record[0]["state"]),
                    "type" => json_encode(!isset($record[0]["type"]) ? "type" : $record[0]["type"]),
                    "type_name" => json_encode(!isset($record[0]["type_name"]) ? "type_name" : $record[0]["type_name"]),
                    "to_check" => json_encode(!isset($record[0]["to_check"]) ? "to_check" : $record[0]["to_check"]),
                    "company_id" => json_encode(!isset($record[0]["company_id"]) ? "company_id" : $record[0]["company_id"]),
                    "company_currency_id" => json_encode(!isset($record[0]["company_currency_id"]) ? "company_currency_id" : $record[0]["company_currency_id"]),
                    "currency_id" => json_encode(!isset($record[0]["currency_id"]) ? "currency_id" : $record[0]["currency_id"]),
                    "line_ids" => json_encode(!isset($record[0]["line_ids"]) ? "line_ids" : $record[0]["line_ids"]),
                    "commercial_partner_id" => json_encode(!isset($record[0]["commercial_partner_id"]) ? "commercial_partner_id" : $record[0]["commercial_partner_id"]),
                    "amount_untaxed" => json_encode(!isset($record[0]["amount_untaxed"]) ? "amount_untaxed" : $record[0]["amount_untaxed"]),
                    "amount_tax" => json_encode(!isset($record[0]["amount_tax"]) ? "amount_tax" : $record[0]["amount_tax"]),
                    "amount_total" => json_encode(!isset($record[0]["amount_total"]) ? "amount_total" : $record[0]["amount_total"]),
                    "amount_residual" => json_encode(!isset($record[0]["amount_residual"]) ? "amount_residual" : $record[0]["amount_residual"]),
                    "amount_untaxed_signed" => json_encode(!isset($record[0]["amount_untaxed_signed"]) ? "amount_untaxed_signed" : $record[0]["amount_untaxed_signed"]),
                    "amount_tax_signed" => json_encode(!isset($record[0]["amount_tax_signed"]) ? "amount_tax_signed" : $record[0]["amount_tax_signed"]),
                    "amount_total_signed" => json_encode(!isset($record[0]["amount_total_signed"]) ? "amount_total_signed" : $record[0]["amount_total_signed"]),
                    "amount_residual_signed" => json_encode(!isset($record[0]["amount_residual_signed"]) ? "amount_residual_signed" : $record[0]["amount_residual_signed"]),
                    "amount_by_group" => json_encode(!isset($record[0]["amount_by_group"]) ? "amount_by_group" : $record[0]["amount_by_group"]),
                    "tax_cash_basis_rec_id" => json_encode(!isset($record[0]["tax_cash_basis_rec_id"]) ? "tax_cash_basis_rec_id" : $record[0]["tax_cash_basis_rec_id"]),
                    "auto_post" => json_encode(!isset($record[0]["auto_post"]) ? "auto_post" : $record[0]["auto_post"]),
                    "invoice_user_id" => json_encode(!isset($record[0]["invoice_user_id"]) ? "invoice_user_id" : $record[0]["invoice_user_id"]),
                    "user_id" => json_encode(!isset($record[0]["user_id"]) ? "user_id" : $record[0]["user_id"]),
                    "invoice_payment_state" => json_encode(!isset($record[0]["invoice_payment_state"]) ? "invoice_payment_state" : $record[0]["invoice_payment_state"]),
                    "invoice_date" => json_encode(!isset($record[0]["invoice_date"]) ? "invoice_date" : $record[0]["invoice_date"]),
                    "invoice_date_due" => json_encode(!isset($record[0]["invoice_date_due"]) ? "invoice_date_due" : $record[0]["invoice_date_due"]),
                    "invoice_payment_ref" => json_encode(!isset($record[0]["invoice_payment_ref"]) ? "invoice_payment_ref" : $record[0]["invoice_payment_ref"]),
                    "invoice_sent" => json_encode(!isset($record[0]["invoice_sent"]) ? "invoice_sent" : $record[0]["invoice_sent"]),
                    "invoice_origin" => json_encode(!isset($record[0]["invoice_origin"]) ? "invoice_origin" : $record[0]["invoice_origin"]),
                    "invoice_line_ids" => json_encode(!isset($record[0]["invoice_line_ids"]) ? "invoice_line_ids" : $record[0]["invoice_line_ids"]),
                    "invoice_incoterm_id" => json_encode(!isset($record[0]["invoice_incoterm_id"]) ? "invoice_incoterm_id" : $record[0]["invoice_incoterm_id"]),
                    "invoice_outstanding_credits_debits_widget" => json_encode(!isset($record[0]["invoice_outstanding_credits_debits_widget"]) ? "invoice_outstanding_credits_debits_widget" : $record[0]["invoice_outstanding_credits_debits_widget"]),
                    "invoice_payments_widget" => json_encode(!isset($record[0]["invoice_payments_widget"]) ? "invoice_payments_widget" : $record[0]["invoice_payments_widget"]),
                    "invoice_has_outstanding" => json_encode(!isset($record[0]["invoice_has_outstanding"]) ? "invoice_has_outstanding" : $record[0]["invoice_has_outstanding"]),
                    "invoice_source_email" => json_encode(!isset($record[0]["invoice_source_email"]) ? "invoice_source_email" : $record[0]["invoice_source_email"]),
                    "invoice_partner_display_name" => json_encode(!isset($record[0]["invoice_partner_display_name"]) ? "invoice_partner_display_name" : $record[0]["invoice_partner_display_name"]),
                    "invoice_partner_icon" => json_encode(!isset($record[0]["invoice_partner_icon"]) ? "invoice_partner_icon" : $record[0]["invoice_partner_icon"]),
                    "invoice_cash_rounding_id" => json_encode(!isset($record[0]["invoice_cash_rounding_id"]) ? "invoice_cash_rounding_id" : $record[0]["invoice_cash_rounding_id"]),
                    "invoice_sequence_number_next" => json_encode(!isset($record[0]["invoice_sequence_number_next"]) ? "invoice_sequence_number_next" : $record[0]["invoice_sequence_number_next"]),
                    "invoice_sequence_number_next_prefix" => json_encode(!isset($record[0]["invoice_sequence_number_next_prefix"]) ? "invoice_sequence_number_next_prefix" : $record[0]["invoice_sequence_number_next_prefix"]),
                    "invoice_filter_type_domain" => json_encode(!isset($record[0]["invoice_filter_type_domain"]) ? "invoice_filter_type_domain" : $record[0]["invoice_filter_type_domain"]),
                    "bank_partner_id" => json_encode(!isset($record[0]["bank_partner_id"]) ? "bank_partner_id" : $record[0]["bank_partner_id"]),
                    "invoice_has_matching_suspense_amount" => json_encode(!isset($record[0]["invoice_has_matching_suspense_amount"]) ? "invoice_has_matching_suspense_amount" : $record[0]["invoice_has_matching_suspense_amount"]),
                    "tax_lock_date_message" => json_encode(!isset($record[0]["tax_lock_date_message"]) ? "tax_lock_date_message" : $record[0]["tax_lock_date_message"]),
                    "has_reconciled_entries" => json_encode(!isset($record[0]["has_reconciled_entries"]) ? "has_reconciled_entries" : $record[0]["has_reconciled_entries"]),
                    "restrict_mode_hash_table" => json_encode(!isset($record[0]["restrict_mode_hash_table"]) ? "restrict_mode_hash_table" : $record[0]["restrict_mode_hash_table"]),
                    "secure_sequence_number" => json_encode(!isset($record[0]["secure_sequence_number"]) ? "secure_sequence_number" : $record[0]["secure_sequence_number"]),
                    "inalterable_hash" => json_encode(!isset($record[0]["inalterable_hash"]) ? "inalterable_hash" : $record[0]["inalterable_hash"]),
                    "string_to_hash" => json_encode(!isset($record[0]["string_to_hash"]) ? "string_to_hash" : $record[0]["string_to_hash"]),
                    "attachment_ids" => json_encode(!isset($record[0]["attachment_ids"]) ? "attachment_ids" : $record[0]["attachment_ids"]),
                    "debit_origin_id" => json_encode(!isset($record[0]["debit_origin_id"]) ? "debit_origin_id" : $record[0]["debit_origin_id"]),
                    "debit_note_ids" => json_encode(!isset($record[0]["debit_note_ids"]) ? "debit_note_ids" : $record[0]["debit_note_ids"]),
                    "debit_note_count" => json_encode(!isset($record[0]["debit_note_count"]) ? "debit_note_count" : $record[0]["debit_note_count"]),
                    "duplicated_vendor_ref" => json_encode(!isset($record[0]["duplicated_vendor_ref"]) ? "duplicated_vendor_ref" : $record[0]["duplicated_vendor_ref"]),
                    "extract_state" => json_encode(!isset($record[0]["extract_state"]) ? "extract_state" : $record[0]["extract_state"]),
                    "extract_status_code" => json_encode(!isset($record[0]["extract_status_code"]) ? "extract_status_code" : $record[0]["extract_status_code"]),
                    "extract_error_message" => json_encode(!isset($record[0]["extract_error_message"]) ? "extract_error_message" : $record[0]["extract_error_message"]),
                    "extract_remote_id" => json_encode(!isset($record[0]["extract_remote_id"]) ? "extract_remote_id" : $record[0]["extract_remote_id"]),
                    "extract_word_ids" => json_encode(!isset($record[0]["extract_word_ids"]) ? "extract_word_ids" : $record[0]["extract_word_ids"]),
                    "extract_can_show_resend_button" => json_encode(!isset($record[0]["extract_can_show_resend_button"]) ? "extract_can_show_resend_button" : $record[0]["extract_can_show_resend_button"]),
                    "extract_can_show_send_button" => json_encode(!isset($record[0]["extract_can_show_send_button"]) ? "extract_can_show_send_button" : $record[0]["extract_can_show_send_button"]),
                    "journal_id" => json_encode(!isset($record[0]["journal_id"]) ? "journal_id" : $record[0]["journal_id"]),
                    "partner_id" => json_encode(!isset($record[0]["partner_id"]) ? "partner_id" : $record[0]["partner_id"]),
                    "fiscal_position_id" => json_encode(!isset($record[0]["fiscal_position_id"]) ? "fiscal_position_id" : $record[0]["fiscal_position_id"]),
                    "invoice_payment_term_id" => json_encode(!isset($record[0]["invoice_payment_term_id"]) ? "invoice_payment_term_id" : $record[0]["invoice_payment_term_id"]),
                    "invoice_partner_bank_id" => json_encode(!isset($record[0]["invoice_partner_bank_id"]) ? "invoice_partner_bank_id" : $record[0]["invoice_partner_bank_id"]),
                    "invoice_vendor_bill_id" => json_encode(!isset($record[0]["invoice_vendor_bill_id"]) ? "invoice_vendor_bill_id" : $record[0]["invoice_vendor_bill_id"]),
                    "l10n_latam_amount_untaxed" => json_encode(!isset($record[0]["l10n_latam_amount_untaxed"]) ? "l10n_latam_amount_untaxed" : $record[0]["l10n_latam_amount_untaxed"]),
                    "l10n_latam_tax_ids" => json_encode(!isset($record[0]["l10n_latam_tax_ids"]) ? "l10n_latam_tax_ids" : $record[0]["l10n_latam_tax_ids"]),
                    "l10n_latam_available_document_type_ids" => json_encode(!isset($record[0]["l10n_latam_available_document_type_ids"]) ? "l10n_latam_available_document_type_ids" : $record[0]["l10n_latam_available_document_type_ids"]),
                    "l10n_latam_document_type_id" => json_encode(!isset($record[0]["l10n_latam_document_type_id"]) ? "l10n_latam_document_type_id" : $record[0]["l10n_latam_document_type_id"]),
                    "l10n_latam_sequence_id" => json_encode(!isset($record[0]["l10n_latam_sequence_id"]) ? "l10n_latam_sequence_id" : $record[0]["l10n_latam_sequence_id"]),
                    "l10n_latam_document_number" => json_encode(!isset($record[0]["l10n_latam_document_number"]) ? "l10n_latam_document_number" : $record[0]["l10n_latam_document_number"]),
                    "l10n_latam_use_documents" => json_encode(!isset($record[0]["l10n_latam_use_documents"]) ? "l10n_latam_use_documents" : $record[0]["l10n_latam_use_documents"]),
                    "l10n_latam_country_code" => json_encode(!isset($record[0]["l10n_latam_country_code"]) ? "l10n_latam_country_code" : $record[0]["l10n_latam_country_code"]),
                    "transaction_ids" => json_encode(!isset($record[0]["transaction_ids"]) ? "transaction_ids" : $record[0]["transaction_ids"]),
                    "authorized_transaction_ids" => json_encode(!isset($record[0]["authorized_transaction_ids"]) ? "authorized_transaction_ids" : $record[0]["authorized_transaction_ids"]),
                    "purchase_vendor_bill_id" => json_encode(!isset($record[0]["purchase_vendor_bill_id"]) ? "purchase_vendor_bill_id" : $record[0]["purchase_vendor_bill_id"]),
                    "purchase_id" => json_encode(!isset($record[0]["purchase_id"]) ? "purchase_id" : $record[0]["purchase_id"]),
                    "stock_move_id" => json_encode(!isset($record[0]["stock_move_id"]) ? "stock_move_id" : $record[0]["stock_move_id"]),
                    "stock_valuation_layer_ids" => json_encode(!isset($record[0]["stock_valuation_layer_ids"]) ? "stock_valuation_layer_ids" : $record[0]["stock_valuation_layer_ids"]),
                    "transfer_model_id" => json_encode(!isset($record[0]["transfer_model_id"]) ? "transfer_model_id" : $record[0]["transfer_model_id"]),
                    "is_tax_closing" => json_encode(!isset($record[0]["is_tax_closing"]) ? "is_tax_closing" : $record[0]["is_tax_closing"]),
                    "tax_report_control_error" => json_encode(!isset($record[0]["tax_report_control_error"]) ? "tax_report_control_error" : $record[0]["tax_report_control_error"]),
                    "internal_notes" => json_encode(!isset($record[0]["internal_notes"]) ? "internal_notes" : $record[0]["internal_notes"]),
                    "reversed_entry_id" => json_encode(!isset($record[0]["reversed_entry_id"]) ? "reversed_entry_id" : $record[0]["reversed_entry_id"]),
                    "team_id" => json_encode(!isset($record[0]["team_id"]) ? "team_id" : $record[0]["team_id"]),
                    "partner_shipping_id" => json_encode(!isset($record[0]["partner_shipping_id"]) ? "partner_shipping_id" : $record[0]["partner_shipping_id"]),
                    "asset_id" => json_encode(!isset($record[0]["asset_id"]) ? "asset_id" : $record[0]["asset_id"]),
                    "asset_asset_type" => json_encode(!isset($record[0]["asset_asset_type"]) ? "asset_asset_type" : $record[0]["asset_asset_type"]),
                    "asset_remaining_value" => json_encode(!isset($record[0]["asset_remaining_value"]) ? "asset_remaining_value" : $record[0]["asset_remaining_value"]),
                    "asset_depreciated_value" => json_encode(!isset($record[0]["asset_depreciated_value"]) ? "asset_depreciated_value" : $record[0]["asset_depreciated_value"]),
                    "asset_manually_modified" => json_encode(!isset($record[0]["asset_manually_modified"]) ? "asset_manually_modified" : $record[0]["asset_manually_modified"]),
                    "asset_value_change" => json_encode(!isset($record[0]["asset_value_change"]) ? "asset_value_change" : $record[0]["asset_value_change"]),
                    "asset_ids" => json_encode(!isset($record[0]["asset_ids"]) ? "asset_ids" : $record[0]["asset_ids"]),
                    "asset_ids_display_name" => json_encode(!isset($record[0]["asset_ids_display_name"]) ? "asset_ids_display_name" : $record[0]["asset_ids_display_name"]),
                    "asset_id_display_name" => json_encode(!isset($record[0]["asset_id_display_name"]) ? "asset_id_display_name" : $record[0]["asset_id_display_name"]),
                    "number_asset_ids" => json_encode(!isset($record[0]["number_asset_ids"]) ? "number_asset_ids" : $record[0]["number_asset_ids"]),
                    "draft_asset_ids" => json_encode(!isset($record[0]["draft_asset_ids"]) ? "draft_asset_ids" : $record[0]["draft_asset_ids"]),
                    "reversal_move_id" => json_encode(!isset($record[0]["reversal_move_id"]) ? "reversal_move_id" : $record[0]["reversal_move_id"]),
                    "move_currency_id" => json_encode(!isset($record[0]["move_currency_id"]) ? "move_currency_id" : $record[0]["move_currency_id"]),
                    "move_inverse_currency_rate" => json_encode(!isset($record[0]["move_inverse_currency_rate"]) ? "move_inverse_currency_rate" : $record[0]["move_inverse_currency_rate"]),
                    "open_move_line_ids" => json_encode(!isset($record[0]["open_move_line_ids"]) ? "open_move_line_ids" : $record[0]["open_move_line_ids"]),
                    "pay_now_journal_id" => json_encode(!isset($record[0]["pay_now_journal_id"]) ? "pay_now_journal_id" : $record[0]["pay_now_journal_id"]),
                    "payment_group_ids" => json_encode(!isset($record[0]["payment_group_ids"]) ? "payment_group_ids" : $record[0]["payment_group_ids"]),
                    "l10n_ar_afip_responsibility_type_id" => json_encode(!isset($record[0]["l10n_ar_afip_responsibility_type_id"]) ? "l10n_ar_afip_responsibility_type_id" : $record[0]["l10n_ar_afip_responsibility_type_id"]),
                    "l10n_ar_currency_rate" => json_encode(!isset($record[0]["l10n_ar_currency_rate"]) ? "l10n_ar_currency_rate" : $record[0]["l10n_ar_currency_rate"]),
                    "l10n_ar_afip_concept" => json_encode(!isset($record[0]["l10n_ar_afip_concept"]) ? "l10n_ar_afip_concept" : $record[0]["l10n_ar_afip_concept"]),
                    "l10n_ar_afip_service_start" => json_encode(!isset($record[0]["l10n_ar_afip_service_start"]) ? "l10n_ar_afip_service_start" : $record[0]["l10n_ar_afip_service_start"]),
                    "l10n_ar_afip_service_end" => json_encode(!isset($record[0]["l10n_ar_afip_service_end"]) ? "l10n_ar_afip_service_end" : $record[0]["l10n_ar_afip_service_end"]),
                    "purchase_order_ids" => json_encode(!isset($record[0]["purchase_order_ids"]) ? "purchase_order_ids" : $record[0]["purchase_order_ids"]),
                    "has_purchases" => json_encode(!isset($record[0]["has_purchases"]) ? "has_purchases" : $record[0]["has_purchases"]),
                    "sale_order_ids" => json_encode(!isset($record[0]["sale_order_ids"]) ? "sale_order_ids" : $record[0]["sale_order_ids"]),
                    "has_sales" => json_encode(!isset($record[0]["has_sales"]) ? "has_sales" : $record[0]["has_sales"]),
                    "rejected_check_id" => json_encode(!isset($record[0]["rejected_check_id"]) ? "rejected_check_id" : $record[0]["rejected_check_id"]),
                    "exchange_diff_adjustment_required" => json_encode(!isset($record[0]["exchange_diff_adjustment_required"]) ? "exchange_diff_adjustment_required" : $record[0]["exchange_diff_adjustment_required"]),
                    "exchange_diff_ignored" => json_encode(!isset($record[0]["exchange_diff_ignored"]) ? "exchange_diff_ignored" : $record[0]["exchange_diff_ignored"]),
                    "exchange_diff_invoice_ids" => json_encode(!isset($record[0]["exchange_diff_invoice_ids"]) ? "exchange_diff_invoice_ids" : $record[0]["exchange_diff_invoice_ids"]),
                    "settled_line_ids" => json_encode(!isset($record[0]["settled_line_ids"]) ? "settled_line_ids" : $record[0]["settled_line_ids"]),
                    "l10n_ar_afip_auth_mode" => json_encode(!isset($record[0]["l10n_ar_afip_auth_mode"]) ? "l10n_ar_afip_auth_mode" : $record[0]["l10n_ar_afip_auth_mode"]),
                    "l10n_ar_afip_auth_code" => json_encode(!isset($record[0]["l10n_ar_afip_auth_code"]) ? "l10n_ar_afip_auth_code" : $record[0]["l10n_ar_afip_auth_code"]),
                    "l10n_ar_afip_auth_code_due" => json_encode(!isset($record[0]["l10n_ar_afip_auth_code_due"]) ? "l10n_ar_afip_auth_code_due" : $record[0]["l10n_ar_afip_auth_code_due"]),
                    "l10n_ar_afip_barcode" => json_encode(!isset($record[0]["l10n_ar_afip_barcode"]) ? "l10n_ar_afip_barcode" : $record[0]["l10n_ar_afip_barcode"]),
                    "l10n_ar_afip_qr_code" => json_encode(!isset($record[0]["l10n_ar_afip_qr_code"]) ? "l10n_ar_afip_qr_code" : $record[0]["l10n_ar_afip_qr_code"]),
                    "l10n_ar_afip_result" => json_encode(!isset($record[0]["l10n_ar_afip_result"]) ? "l10n_ar_afip_result" : $record[0]["l10n_ar_afip_result"]),
                    "l10n_ar_afip_ws" => json_encode(!isset($record[0]["l10n_ar_afip_ws"]) ? "l10n_ar_afip_ws" : $record[0]["l10n_ar_afip_ws"]),
                    "l10n_ar_afip_verification_type" => json_encode(!isset($record[0]["l10n_ar_afip_verification_type"]) ? "l10n_ar_afip_verification_type" : $record[0]["l10n_ar_afip_verification_type"]),
                    "l10n_ar_afip_verification_result" => json_encode(!isset($record[0]["l10n_ar_afip_verification_result"]) ? "l10n_ar_afip_verification_result" : $record[0]["l10n_ar_afip_verification_result"]),
                    "l10n_ar_afip_fce_is_cancellation" => json_encode(!isset($record[0]["l10n_ar_afip_fce_is_cancellation"]) ? "l10n_ar_afip_fce_is_cancellation" : $record[0]["l10n_ar_afip_fce_is_cancellation"]),
                    "sale_type_id" => json_encode(!isset($record[0]["sale_type_id"]) ? "sale_type_id" : $record[0]["sale_type_id"]),
                    "purchase_order_number" => json_encode(!isset($record[0]["purchase_order_number"]) ? "purchase_order_number" : $record[0]["purchase_order_number"]),
                    "timesheet_ids" => json_encode(!isset($record[0]["timesheet_ids"]) ? "timesheet_ids" : $record[0]["timesheet_ids"]),
                    "timesheet_count" => json_encode(!isset($record[0]["timesheet_count"]) ? "timesheet_count" : $record[0]["timesheet_count"]),
                    "picking_ids" => json_encode(!isset($record[0]["picking_ids"]) ? "picking_ids" : $record[0]["picking_ids"]),
                    "computed_currency_rate" => json_encode(!isset($record[0]["computed_currency_rate"]) ? "computed_currency_rate" : $record[0]["computed_currency_rate"]),
                    "l10n_ar_afip_asoc_period_start" => json_encode(!isset($record[0]["l10n_ar_afip_asoc_period_start"]) ? "l10n_ar_afip_asoc_period_start" : $record[0]["l10n_ar_afip_asoc_period_start"]),
                    "l10n_ar_afip_asoc_period_end" => json_encode(!isset($record[0]["l10n_ar_afip_asoc_period_end"]) ? "l10n_ar_afip_asoc_period_end" : $record[0]["l10n_ar_afip_asoc_period_end"]),
                    "display_currency_alert" => json_encode(!isset($record[0]["display_currency_alert"]) ? "display_currency_alert" : $record[0]["display_currency_alert"]),
                    "document_type_internal_type" => json_encode(!isset($record[0]["document_type_internal_type"]) ? "document_type_internal_type" : $record[0]["document_type_internal_type"]),
                    "invoice_validation_type" => json_encode(!isset($record[0]["invoice_validation_type"]) ? "invoice_validation_type" : $record[0]["invoice_validation_type"]),
                    "campaign_id" => json_encode(!isset($record[0]["campaign_id"]) ? "campaign_id" : $record[0]["campaign_id"]),
                    "source_id" => json_encode(!isset($record[0]["source_id"]) ? "source_id" : $record[0]["source_id"]),
                    "medium_id" => json_encode(!isset($record[0]["medium_id"]) ? "medium_id" : $record[0]["medium_id"]),
                    "activity_exception_decoration" => json_encode(!isset($record[0]["activity_exception_decoration"]) ? "activity_exception_decoration" : $record[0]["activity_exception_decoration"]),
                    "activity_exception_icon" => json_encode(!isset($record[0]["activity_exception_icon"]) ? "activity_exception_icon" : $record[0]["activity_exception_icon"]),
                    "activity_ids" => json_encode(!isset($record[0]["activity_ids"]) ? "activity_ids" : $record[0]["activity_ids"]),
                    "activity_state" => json_encode(!isset($record[0]["activity_state"]) ? "activity_state" : $record[0]["activity_state"]),
                    "activity_user_id" => json_encode(!isset($record[0]["activity_user_id"]) ? "activity_user_id" : $record[0]["activity_user_id"]),
                    "activity_type_id" => json_encode(!isset($record[0]["activity_type_id"]) ? "activity_type_id" : $record[0]["activity_type_id"]),
                    "activity_date_deadline" => json_encode(!isset($record[0]["activity_date_deadline"]) ? "activity_date_deadline" : $record[0]["activity_date_deadline"]),
                    "activity_summary" => json_encode(!isset($record[0]["activity_summary"]) ? "activity_summary" : $record[0]["activity_summary"]),
                    "message_is_follower" => json_encode(!isset($record[0]["message_is_follower"]) ? "message_is_follower" : $record[0]["message_is_follower"]),
                    "message_follower_ids" => json_encode(!isset($record[0]["message_follower_ids"]) ? "message_follower_ids" : $record[0]["message_follower_ids"]),
                    "message_partner_ids" => json_encode(!isset($record[0]["message_partner_ids"]) ? "message_partner_ids" : $record[0]["message_partner_ids"]),
                    "message_channel_ids" => json_encode(!isset($record[0]["message_channel_ids"]) ? "message_channel_ids" : $record[0]["message_channel_ids"]),
                    "message_ids" => json_encode(!isset($record[0]["message_ids"]) ? "message_ids" : $record[0]["message_ids"]),
                    "message_unread" => json_encode(!isset($record[0]["message_unread"]) ? "message_unread" : $record[0]["message_unread"]),
                    "message_unread_counter" => json_encode(!isset($record[0]["message_unread_counter"]) ? "message_unread_counter" : $record[0]["message_unread_counter"]),
                    "message_needaction" => json_encode(!isset($record[0]["message_needaction"]) ? "message_needaction" : $record[0]["message_needaction"]),
                    "message_needaction_counter" => json_encode(!isset($record[0]["message_needaction_counter"]) ? "message_needaction_counter" : $record[0]["message_needaction_counter"]),
                    "message_has_error" => json_encode(!isset($record[0]["message_has_error"]) ? "message_has_error" : $record[0]["message_has_error"]),
                    "message_has_error_counter" => json_encode(!isset($record[0]["message_has_error_counter"]) ? "message_has_error_counter" : $record[0]["message_has_error_counter"]),
                    "message_main_attachment_id" => json_encode(!isset($record[0]["message_main_attachment_id"]) ? "message_main_attachment_id" : $record[0]["message_main_attachment_id"]),
                    "failed_message_ids" => json_encode(!isset($record[0]["failed_message_ids"]) ? "failed_message_ids" : $record[0]["failed_message_ids"]),
                    "website_message_ids" => json_encode(!isset($record[0]["website_message_ids"]) ? "website_message_ids" : $record[0]["website_message_ids"]),
                    "message_has_sms_error" => json_encode(!isset($record[0]["message_has_sms_error"]) ? "message_has_sms_error" : $record[0]["message_has_sms_error"]),
                    "message_attachment_count" => json_encode(!isset($record[0]["message_attachment_count"]) ? "message_attachment_count" : $record[0]["message_attachment_count"]),
                    "access_url" => json_encode(!isset($record[0]["access_url"]) ? "access_url" : $record[0]["access_url"]),
                    "access_token" => json_encode(!isset($record[0]["access_token"]) ? "access_token" : $record[0]["access_token"]),
                    "access_warning" => json_encode(!isset($record[0]["access_warning"]) ? "access_warning" : $record[0]["access_warning"]),
                    "smart_search" => json_encode(!isset($record[0]["smart_search"]) ? "smart_search" : $record[0]["smart_search"]),
                    "account_move_id" => json_encode(!isset($record[0]["id"]) ?   "id" : $record[0]["id"]),
                    "display_name" => json_encode(!isset($record[0]["display_name"]) ? "display_name" : $record[0]["display_name"]),
                    "create_uid" => json_encode(!isset($record[0]["create_uid"]) ? "create_uid" : $record[0]["create_uid"]),
                    "create_date" => json_encode(!isset($record[0]["create_date"]) ? "create_date" : $record[0]["create_date"]),
                    "write_uid" => json_encode(!isset($record[0]["write_uid"]) ? "write_uid" : $record[0]["write_uid"]),
                    "write_date" => json_encode(!isset($record[0]["write_date"]) ? "write_date" : $record[0]["write_date"]),
                    "__last_update" => json_encode(!isset($record[0]["__last_update"]) ? "__last_update" : $record[0]["__last_update"]),
                ]);
            }
        }

        return $this->successResponse("Finalizado carga de datos a la tabla account_moves", 200);
    }
}
