<?php

namespace App\Http\Controllers;

use App\Models\ProductTemplateAttributeLine;
use Illuminate\Http\Request;
use ripcord;

class VarianteDetalleController extends ApiController
{
    //
    public function index()
    {
        $url = env('URL_DATABASE_ODDO');
        $url_auth = $url . '/xmlrpc/2/common';
        $url_exec = $url . '/xmlrpc/2/object';
        $db = env('DATABASE_ODDO');
        $username =  env('USER_ODDO');
        $password = env('PASSWORD_ODDO');
        $common = ripcord::client($url_auth);
        $uid = $common->authenticate($db, $username, $password, array());

        $models = ripcord::client($url_exec);

        $ids = $models->execute_kw(
            $db,
            $uid,
            $password,
            'product.template.attribute.line',
            'search',
            array(array())
        );

        $collection = collect($ids);

        ProductTemplateAttributeLine::truncate();

        foreach ($collection->chunk(200) as $key => $idschunks) {
            foreach ($idschunks->toArray() as $prod) {
                $record = $models->execute_kw(
                    $db,
                    $uid,
                    $password,
                    'product.template.attribute.line',
                    'read',
                    array($prod),

                    array('fields' => array(
                        "active",
                        "product_tmpl_id",
                        "attribute_id",
                        "value_ids",
                        "product_template_value_ids",
                        "smart_search",
                        "id",
                        "display_name",
                        "create_uid",
                        "create_date",
                        "write_uid",
                        "write_date",
                        "__last_update",
                    ))
                );

                ProductTemplateAttributeLine::create([
                    "active" => json_encode(!isset($record[0]["active"]) ? "active" : $record[0]["active"]),
                    "product_tmpl_id" => json_encode(!isset($record[0]["product_tmpl_id"]) ? "produc?_tmpl_id" : $record[0]["product_tmpl_id"]),
                    "attribute_id" => json_encode(!isset($record[0]["attribute_id"]) ? "att?ibute_id" : $record[0]["attribute_id"]),
                    "value_ids" => json_encode(!isset($record[0]["value_ids"]) ? "?alue_ids" : $record[0]["value_ids"]),
                    "product_template_value_ids" => json_encode(!isset($record[0]["product_template_value_ids"]) ? "product_template_?alue_ids" : $record[0]["product_template_value_ids"]),
                    "smart_search" => json_encode(!isset($record[0]["smart_search"]) ? "sma?t_search" : $record[0]["smart_search"]),
                    "product_template_attribute_line_id" => json_encode(!isset($record[0]["id"]) ? "id" : $record[0]["id"]),
                    "display_name" => json_encode(!isset($record[0]["display_name"]) ? "dis?lay_name" : $record[0]["display_name"]),
                    "create_uid" => json_encode(!isset($record[0]["create_uid"]) ? "c?eate_uid" : $record[0]["create_uid"]),
                    "create_date" => json_encode(!isset($record[0]["create_date"]) ? "cr?ate_date" : $record[0]["create_date"]),
                    "write_uid" => json_encode(!isset($record[0]["write_uid"]) ? "?rite_uid" : $record[0]["write_uid"]),
                    "write_date" => json_encode(!isset($record[0]["write_date"]) ? "w?ite_date" : $record[0]["write_date"]),
                    "__last_update" => json_encode(!isset($record[0]["__last_update"]) ? "__la?t_update" : $record[0]["__last_update"]),
                ]);
            }
        }

        return $this->successResponse("Finalizado carga de datos a la tabla product_template_attribute_lines", 200);
    }
}
