<?php

namespace App\Http\Controllers;

use App\Models\StockValuationLayer;
use Illuminate\Http\Request;
use ripcord;

class StockDetalleController extends ApiController
{
    //
    public function index()
    {

        $url = env('URL_DATABASE_ODDO');
        $url_auth = $url . '/xmlrpc/2/common';
        $url_exec = $url . '/xmlrpc/2/object';
        $db = env('DATABASE_ODDO');
        $username =  env('USER_ODDO');
        $password = env('PASSWORD_ODDO');
        $common = ripcord::client($url_auth);
        $uid = $common->authenticate($db, $username, $password, array());

        $models = ripcord::client($url_exec);

        $ids = $models->execute_kw(
            $db,
            $uid,
            $password,
            'stock.valuation.layer',
            'search',
            array(array())
        );

        $collection = collect($ids);

        StockValuationLayer::truncate();

        foreach ($collection->chunk(200) as $key => $idschunks) {
            sleep(10);
            foreach ($idschunks->toArray() as $prod) {
                $record = $models->execute_kw(
                    $db,
                    $uid,
                    $password,
                    'stock.valuation.layer',
                    'read',
                    array($prod),

                    array('fields' => array(
                        "active",
                        "company_id",
                        "product_id",
                        "categ_id",
                        "product_tmpl_id",
                        "quantity",
                        "uom_id",
                        "currency_id",
                        "unit_cost",
                        "value",
                        "remaining_qty",
                        "remaining_value",
                        "description",
                        "stock_valuation_layer_id",
                        "stock_valuation_layer_ids",
                        "stock_move_id",
                        "account_move_id",
                        "smart_search",
                        "id",
                        "display_name",
                        "create_uid",
                        "create_date",
                        "write_uid",
                        "write_date",
                        "__last_update",
                    ))
                );

                StockValuationLayer::create([
                    "active" => json_encode(!isset($record[0]["active"]) ? "active" : $record[0]["active"]),
                    "company_id" => json_encode(!isset($record[0]["company_id"]) ? "company_id" : $record[0]["company_id"]),
                    "product_id" => json_encode(!isset($record[0]["product_id"]) ? "product_id" : $record[0]["product_id"]),
                    "categ_id" => json_encode(!isset($record[0]["categ_id"]) ? "categ_id" : $record[0]["categ_id"]),
                    "product_tmpl_id" => json_encode(!isset($record[0]["product_tmpl_id"]) ? "product_tmpl_id" : $record[0]["product_tmpl_id"]),
                    "quantity" => json_encode(!isset($record[0]["quantity"]) ? "quantity" : $record[0]["quantity"]),
                    "uom_id" => json_encode(!isset($record[0]["uom_id"]) ? "uom_id" : $record[0]["uom_id"]),
                    "currency_id" => json_encode(!isset($record[0]["currency_id"]) ? "currency_id" : $record[0]["currency_id"]),
                    "unit_cost" => json_encode(!isset($record[0]["unit_cost"]) ? "unit_cost" : $record[0]["unit_cost"]),
                    "value" => json_encode(!isset($record[0]["value"]) ? "value" : $record[0]["value"]),
                    "remaining_qty" => json_encode(!isset($record[0]["remaining_qty"]) ? "remaining_qty" : $record[0]["remaining_qty"]),
                    "remaining_value" => json_encode(!isset($record[0]["remaining_value"]) ? "remaining_value" : $record[0]["remaining_value"]),
                    "description" => json_encode(!isset($record[0]["description"]) ? "description" : $record[0]["description"]),
                    "stock_valuation_layer_id" => json_encode(!isset($record[0]["stock_valuation_layer_id"]) ? "stock_valuation_layer_id" : $record[0]["stock_valuation_layer_id"]),
                    "stock_valuation_layer_ids" => json_encode(!isset($record[0]["stock_valuation_layer_ids"]) ? "stock_valuation_layer_ids" : $record[0]["stock_valuation_layer_ids"]),
                    "stock_move_id" => json_encode(!isset($record[0]["stock_move_id"]) ? "stock_move_id" : $record[0]["stock_move_id"]),
                    "account_move_id" => json_encode(!isset($record[0]["account_move_id"]) ? "account_move_id" : $record[0]["account_move_id"]),
                    "smart_search" => json_encode(!isset($record[0]["smart_search"]) ? "smart_search" : $record[0]["smart_search"]),
                    "stock_valuation_layer_app_id" => json_encode(!isset($record[0]["id"]) ? "id" : $record[0]["id"]),
                    "display_name" => json_encode(!isset($record[0]["display_name"]) ? "display_name" : $record[0]["display_name"]),
                    "create_uid" => json_encode(!isset($record[0]["create_uid"]) ? "create_uid" : $record[0]["create_uid"]),
                    "create_date" => json_encode(!isset($record[0]["create_date"]) ? "create_date" : $record[0]["create_date"]),
                    "write_uid" => json_encode(!isset($record[0]["write_uid"]) ? "write_uid" : $record[0]["write_uid"]),
                    "write_date" => json_encode(!isset($record[0]["write_date"]) ? "write_date" : $record[0]["write_date"]),
                    "__last_update" => json_encode(!isset($record[0]["__last_update"]) ? "__last_update" : $record[0]["__last_update"]),
                ]);
            }
        }

        return $this->successResponse("Finalizado carga de datos a la tabla product_template_attribute_lines", 200);
    }
}
