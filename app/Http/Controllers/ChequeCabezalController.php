<?php

namespace App\Http\Controllers;

use App\Models\AccountCheck;
use Illuminate\Http\Request;
use ripcord;

class ChequeCabezalController extends ApiController
{
    //
    public function index()
    {
        ini_set('memory_limit', '2048M');

        //$url = 'https://train-quimicamineral-07-07-4.nubeadhoc.com';
        $url = env('URL_DATABASE_ODDO');
        $url_auth = $url . '/xmlrpc/2/common';
        $url_exec = $url . '/xmlrpc/2/object';
        $db = env('DATABASE_ODDO');
        $username =  env('USER_ODDO');
        $password = env('PASSWORD_ODDO');
        $common = ripcord::client($url_auth);
        $uid = $common->authenticate($db, $username, $password, array());
        $models = ripcord::client($url_exec);
        // $fields = $models->execute_kw($db, $uid, $password,
        // 'account.check', 'fields_get',
        // array(), array('attributes' => array('string', 'type')));
        // dd($fields);

        // $count = $models->execute_kw($db, $uid, $password,
        //                             'account.check', 'search_count',
        //                             array(array(array('id', '>', -1)))
        //                         );
        // dd($count);
        $ids = $models->execute_kw(
            $db,
            $uid,
            $password,
            'account.check',
            'search',
            array(array())
        );

        $collection = collect($ids);

        AccountCheck::truncate();

        foreach ($collection->chunk(20) as $idschunks) {
            sleep(10);
            foreach ($idschunks->toArray() as $id) {

                $record = $models->execute_kw(
                    $db,
                    $uid,
                    $password,
                    'account.check',
                    'read',
                    [$id],
                    array('fields' => array(
                        "operation_ids",
                        "name",
                        "number",
                        "checkbook_id",
                        "issue_check_subtype",
                        "type",
                        "partner_id",
                        "first_partner_id",
                        "state",
                        "issue_date",
                        "owner_vat",
                        "owner_name",
                        "bank_id",
                        "amount",
                        "amount_company_currency",
                        "currency_id",
                        "payment_date",
                        "journal_id",
                        "company_id",
                        "company_currency_id",
                        "activity_exception_decoration",
                        "activity_exception_icon",
                        "activity_ids",
                        "activity_state",
                        "activity_user_id",
                        "activity_type_id",
                        "activity_date_deadline",
                        "activity_summary",
                        "message_is_follower",
                        "message_follower_ids",
                        "message_partner_ids",
                        "message_channel_ids",
                        "message_ids",
                        "message_unread",
                        "message_unread_counter",
                        "message_needaction",
                        "message_needaction_counter",
                        "message_has_error",
                        "message_has_error_counter",
                        "message_main_attachment_id",
                        "failed_message_ids",
                        "website_message_ids",
                        "message_has_sms_error",
                        "message_attachment_count",
                        "smart_search",
                        "id",
                        "display_name",
                        "create_uid",
                        "create_date",
                        "write_uid",
                        "write_date",
                        "__last_update",
                    ))
                );

                AccountCheck::create([
                    "operation_ids" => json_encode(!isset($record[0]["operation_ids"]) ? "operation_ids" : $record[0]["operation_ids"]),
                    "name" => json_encode(!isset($record[0]["name"]) ? "name" : $record[0]["name"]),
                    "number" => json_encode(!isset($record[0]["number"]) ? "number" : $record[0]["number"]),
                    "checkbook_id" => json_encode(!isset($record[0]["checkbook_id"]) ? "checkbook_id" : $record[0]["checkbook_id"]),
                    "issue_check_subtype" => json_encode(!isset($record[0]["issue_check_subtype"]) ? "issue_check_subtype" : $record[0]["issue_check_subtype"]),
                    "type" => json_encode(!isset($record[0]["type"]) ? "type" : $record[0]["type"]),
                    "partner_id" => json_encode(!isset($record[0]["partner_id"]) ? "partner_id" : $record[0]["partner_id"]),
                    "first_partner_id" => json_encode(!isset($record[0]["first_partner_id"]) ? "first_partner_id" : $record[0]["first_partner_id"]),
                    "state" => json_encode(!isset($record[0]["state"]) ? "state" : $record[0]["state"]),
                    "issue_date" => json_encode(!isset($record[0]["issue_date"]) ? "issue_date" : $record[0]["issue_date"]),
                    "owner_vat" => json_encode(!isset($record[0]["owner_vat"]) ? "owner_vat" : $record[0]["owner_vat"]),
                    "owner_name" => json_encode(!isset($record[0]["owner_name"]) ? "owner_name" : $record[0]["owner_name"]),
                    "bank_id" => json_encode(!isset($record[0]["bank_id"]) ? "bank_id" : $record[0]["bank_id"]),
                    "amount" => json_encode(!isset($record[0]["amount"]) ? "amount" : $record[0]["amount"]),
                    "amount_company_currency" => json_encode(!isset($record[0]["amount_company_currency"]) ? "amount_company_currency" : $record[0]["amount_company_currency"]),
                    "currency_id" => json_encode(!isset($record[0]["currency_id"]) ? "currency_id" : $record[0]["currency_id"]),
                    "payment_date" => json_encode(!isset($record[0]["payment_date"]) ? "payment_date" : $record[0]["payment_date"]),
                    "journal_id" => json_encode(!isset($record[0]["journal_id"]) ? "journal_id" : $record[0]["journal_id"]),
                    "company_id" => json_encode(!isset($record[0]["company_id"]) ? "company_id" : $record[0]["company_id"]),
                    "company_currency_id" => json_encode(!isset($record[0]["company_currency_id"]) ? "company_currency_id" : $record[0]["company_currency_id"]),
                    "activity_exception_decoration" => json_encode(!isset($record[0]["activity_exception_decoration"]) ? "activity_exception_decoration" : $record[0]["activity_exception_decoration"]),
                    "activity_exception_icon" => json_encode(!isset($record[0]["activity_exception_icon"]) ? "activity_exception_icon" : $record[0]["activity_exception_icon"]),
                    "activity_ids" => json_encode(!isset($record[0]["activity_ids"]) ? "activity_ids" : $record[0]["activity_ids"]),
                    "activity_state" => json_encode(!isset($record[0]["activity_state"]) ? "activity_state" : $record[0]["activity_state"]),
                    "activity_user_id" => json_encode(!isset($record[0]["activity_user_id"]) ? "activity_user_id" : $record[0]["activity_user_id"]),
                    "activity_type_id" => json_encode(!isset($record[0]["activity_type_id"]) ? "activity_type_id" : $record[0]["activity_type_id"]),
                    "activity_date_deadline" => json_encode(!isset($record[0]["activity_date_deadline"]) ? "activity_date_deadline" : $record[0]["activity_date_deadline"]),
                    "activity_summary" => json_encode(!isset($record[0]["activity_summary"]) ? "activity_summary" : $record[0]["activity_summary"]),
                    "message_is_follower" => json_encode(!isset($record[0]["message_is_follower"]) ? "message_is_follower" : $record[0]["message_is_follower"]),
                    "message_follower_ids" => json_encode(!isset($record[0]["message_follower_ids"]) ? "message_follower_ids" : $record[0]["message_follower_ids"]),
                    "message_partner_ids" => json_encode(!isset($record[0]["message_partner_ids"]) ? "message_partner_ids" : $record[0]["message_partner_ids"]),
                    "message_channel_ids" => json_encode(!isset($record[0]["message_channel_ids"]) ? "message_channel_ids" : $record[0]["message_channel_ids"]),
                    "message_ids" => json_encode(!isset($record[0]["message_ids"]) ? "message_ids" : $record[0]["message_ids"]),
                    "message_unread" => json_encode(!isset($record[0]["message_unread"]) ? "message_unread" : $record[0]["message_unread"]),
                    "message_unread_counter" => json_encode(!isset($record[0]["message_unread_counter"]) ? "message_unread_counter" : $record[0]["message_unread_counter"]),
                    "message_needaction" => json_encode(!isset($record[0]["message_needaction"]) ? "message_needaction" : $record[0]["message_needaction"]),
                    "message_needaction_counter" => json_encode(!isset($record[0]["message_needaction_counter"]) ? "message_needaction_counter" : $record[0]["message_needaction_counter"]),
                    "message_has_error" => json_encode(!isset($record[0]["message_has_error"]) ? "message_has_error" : $record[0]["message_has_error"]),
                    "message_has_error_counter" => json_encode(!isset($record[0]["message_has_error_counter"]) ? "message_has_error_counter" : $record[0]["message_has_error_counter"]),
                    "message_main_attachment_id" => json_encode(!isset($record[0]["message_main_attachment_id"]) ? "message_main_attachment_id" : $record[0]["message_main_attachment_id"]),
                    "failed_message_ids" => json_encode(!isset($record[0]["failed_message_ids"]) ? "failed_message_ids" : $record[0]["failed_message_ids"]),
                    "website_message_ids" => json_encode(!isset($record[0]["website_message_ids"]) ? "website_message_ids" : $record[0]["website_message_ids"]),
                    "message_has_sms_error" => json_encode(!isset($record[0]["message_has_sms_error"]) ? "message_has_sms_error" : $record[0]["message_has_sms_error"]),
                    "message_attachment_count" => json_encode(!isset($record[0]["message_attachment_count"]) ? "message_attachment_count" : $record[0]["message_attachment_count"]),
                    "smart_search" => json_encode(!isset($record[0]["smart_search"]) ? "smart_search" : $record[0]["smart_search"]),
                    "account_check_id" => json_encode(!isset($record[0]["id"]) ? "id":  $record[0]["id"]),
                    "display_name" => json_encode(!isset($record[0]["display_name"]) ? "display_name" : $record[0]["display_name"]),
                    "create_uid" => json_encode(!isset($record[0]["create_uid"]) ? "create_uid" : $record[0]["create_uid"]),
                    "create_date" => json_encode(!isset($record[0]["create_date"]) ? "create_date" : $record[0]["create_date"]),
                    "write_uid" => json_encode(!isset($record[0]["write_uid"]) ? "write_uid" : $record[0]["write_uid"]),
                    "write_date" => json_encode(!isset($record[0]["write_date"]) ? "write_date" : $record[0]["write_date"]),
                    "__last_update" => json_encode(!isset($record[0]["__last_update"]) ? "__last_update" : $record[0]["__last_update"]),
                ]);
            }
        }


        return $this->successResponse("Finalizado carga de datos a la tabla account_checks", 200);
    }
}
