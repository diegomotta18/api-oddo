<?php

namespace App\Http\Controllers;

use App\Models\Rescurrencyrate;
use ripcord;

class RescurrencyrateController extends Controller
{
    //
    public function index()
    {
        $url = env('URL_DATABASE_ODDO');
        $url_auth = $url . '/xmlrpc/2/common';
        $url_exec = $url . '/xmlrpc/2/object';
        $db = env('DATABASE_ODDO');
        $username =  env('USER_ODDO');
        $password = env('PASSWORD_ODDO');
        $common = ripcord::client($url_auth);
        $uid = $common->authenticate($db, $username, $password, array());
        $models = ripcord::client($url_exec);
//        $fields = $models->execute_kw($db, $uid, $password,
//            'res.currency.rate', 'fields_get',
//            array(), array('attributes' => array('string', 'type')));
//        dd($fields);
        $ids = $models->execute_kw($db, $uid, $password,
            'res.currency.rate', 'search',
            array(array())
        );

        $collection = collect($ids);

        Rescurrencyrate::truncate();
        foreach ($collection->chunk(100) as $idschunks) {
            sleep(10);
            foreach ($idschunks->toArray() as $id) {
                $record = $models->execute_kw($db, $uid, $password,
                    'res.currency.rate',
                    'read',
                    [$id],
                    array('fields' => array(
                        "name",
                        "rate",
                        "currency_id",
                        "company_id",
                        "inverse_rate",
                        "smart_search",
                        "id",
                        "display_name",
                        "create_uid",
                        "create_date",
                        "write_uid",
                        "write_date",
                        "__last_update",
                    )));

                Rescurrencyrate::create([
                    "name" => json_encode(!isset($record[0]["name"]) ? "name" : $record[0]["name"]),
                    "rate" => json_encode(!isset($record[0]["rate"]) ? "rate" : $record[0]["rate"]),
                    "currency_id" => json_encode(!isset($record[0]["currency_id"]) ? "currency_id" : $record[0]["currency_id"]),
                    "company_id" => json_encode(!isset($record[0]["company_id"]) ? "company_id" : $record[0]["company_id"]),
                    "inverse_rate" => json_encode(!isset($record[0]["inverse_rate"]) ? "inverse_rate" : $record[0]["inverse_rate"]),
                    "smart_search" => json_encode(!isset($record[0]["smart_search"]) ? "smart_search" : $record[0]["smart_search"]),
                    "rescurrencyrate_id" => json_encode(!isset($record[0]["id"]) ? "id" : $record[0]["id"]),
                    "display_name" => json_encode(!isset($record[0]["display_name"]) ? "display_name" : $record[0]["display_name"]),
                    "create_uid" => json_encode(!isset($record[0]["create_uid"]) ? "create_uid" : $record[0]["create_uid"]),
                    "create_date" => json_encode(!isset($record[0]["create_date"]) ? "create_date" : $record[0]["create_date"]),
                    "write_uid" => json_encode(!isset($record[0]["write_uid"]) ? "write_uid" : $record[0]["write_uid"]),
                    "write_date" => json_encode(!isset($record[0]["write_date"]) ? "write_date" : $record[0]["write_date"]),
                    "__last_update" => json_encode(!isset($record[0]["__last_update"]) ? "__last_update" : $record[0]["__last_update"]),
                ]);
            }

        }
        return $this->successResponse("Finalizado carga de datos a la tabla product_template_attribute_lines", 200);

    }
}
