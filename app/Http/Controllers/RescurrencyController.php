<?php

namespace App\Http\Controllers;

use App\Models\Client;
use App\Models\Rescurrency;
use Illuminate\Http\Request;
use ripcord;

class RescurrencyController extends ApiController
{
    //
    public function index()
    {
        $url = env('URL_DATABASE_ODDO');
        $url_auth = $url . '/xmlrpc/2/common';
        $url_exec = $url . '/xmlrpc/2/object';
        $db = env('DATABASE_ODDO');
        $username =  env('USER_ODDO');
        $password = env('PASSWORD_ODDO');
        $common = ripcord::client($url_auth);
        $uid = $common->authenticate($db, $username, $password, array());
        $models = ripcord::client($url_exec);
//         $fields = $models->execute_kw($db, $uid, $password,
//         'res.currency', 'fields_get',
//         array(), array('attributes' => array('string', 'type')));
//         dd($fields);

        $ids = $models->execute_kw($db, $uid, $password,
            'res.currency', 'search',
            array(array())
        );

        $collection = collect($ids);

        Rescurrency::truncate();

        foreach($collection->chunk(100) as $idschunks) {
            sleep(10);
            foreach ($idschunks->toArray() as $id) {
                $record = $models->execute_kw($db, $uid, $password,
                    'res.currency',
                    'read',
                    [$id],
                    array('fields' => array(
                        "name",
                        "symbol",
                        "rate",
                        "rate_ids",
                        "rounding",
                        "decimal_places",
                        "active",
                        "position",
                        "date",
                        "currency_unit_label",
                        "currency_subunit_label",
                        "inverse_rate",
                        "l10n_ar_afip_code",
                        "smart_search",
                        "id",
                        "display_name",
                        "create_uid",
                        "create_date",
                        "write_uid",
                        "write_date",
                        "__last_update"
                    )));

                Rescurrency::create([
                    "name"=> json_encode(!isset($record[0]["name"]) ? "" : $record[0]["name"]),
                    "symbol"=> json_encode(!isset($record[0]["symbol"]) ? "" : $record[0]["symbol"]),
                    "rate"=> json_encode(!isset($record[0]["rate"]) ? "" : $record[0]["rate"]),
                    "rate_ids"=> json_encode(!isset($record[0]["rate_ids"]) ? "" : $record[0]["rate_ids"]),
                    "rounding"=> json_encode(!isset($record[0]["rounding"]) ? "" : $record[0]["rounding"]),
                    "decimal_places"=> json_encode(!isset($record[0]["decimal_places"]) ? "" : $record[0]["decimal_places"]),
                    "active"=> json_encode(!isset($record[0]["active"]) ? "" : $record[0]["active"]),
                    "position"=> json_encode(!isset($record[0]["position"]) ? "" : $record[0]["position"]),
                    "date"=> json_encode(!isset($record[0]["date"]) ? "" : $record[0]["date"]),
                    "currency_unit_label"=> json_encode(!isset($record[0]["currency_unit_label"]) ? "" : $record[0]["currency_unit_label"]),
                    "currency_subunit_label"=> json_encode(!isset($record[0]["currency_subunit_label"]) ? "" : $record[0]["currency_subunit_label"]),
                    "inverse_rate"=> json_encode(!isset($record[0]["inverse_rate"]) ? "" : $record[0]["inverse_rate"]),
                    "l10n_ar_afip_code"=> json_encode(!isset($record[0]["l10n_ar_afip_code"]) ? "" : $record[0]["l10n_ar_afip_code"]),
                    "smart_search"=> json_encode(!isset($record[0]["smart_search"]) ? "" : $record[0]["smart_search"]),
                    "rescurrency_id"=> json_encode(!isset($record[0]["id"]) ? "" : $record[0]["id"]),
                    "display_name"=> json_encode(!isset($record[0]["display_name"]) ? "" : $record[0]["display_name"]),
                    "create_uid" => json_encode(!isset($record[0]["create_uid" ]) ? "" : $record[0]["create_uid" ]),
                    "create_date" => json_encode(!isset($record[0]["create_date" ]) ? "" : $record[0]["create_date" ]),
                    "write_uid"=> json_encode(!isset($record[0]["write_uid"]) ? "" : $record[0]["write_uid"]),
                    "write_date"=> json_encode(!isset($record[0]["write_date"]) ? "" : $record[0]["write_date"]),
                    "__last_update"=> json_encode(!isset($record[0]["__last_update"]) ? "" : $record[0]["__last_update"]),
                ]);
            }
        }

        return $this->successResponse("Finalizado carga de datos a la tabla res.currency", 200);

    }
}
