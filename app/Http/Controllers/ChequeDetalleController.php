<?php

namespace App\Http\Controllers;

use App\Models\AccountCheckOperation;
use Illuminate\Http\Request;
use ripcord;

class ChequeDetalleController extends ApiController
{
    //
    public function index()
    {

        ini_set('memory_limit', '-1');

        //$url = 'https://train-quimicamineral-07-07-4.nubeadhoc.com';
        $url = env('URL_DATABASE_ODDO');
        $url_auth = $url . '/xmlrpc/2/common';
        $url_exec = $url . '/xmlrpc/2/object';
        $db = env('DATABASE_ODDO');
        $username =  env('USER_ODDO');
        $password = env('PASSWORD_ODDO');
        $common = ripcord::client($url_auth);
        $uid = $common->authenticate($db, $username, $password, array());

        $models = ripcord::client($url_exec);

        $ids = $models->execute_kw(
            $db,
            $uid,
            $password,
            'account.check.operation',
            'search',
            array(array())
        );

        $collection = collect($ids);

        AccountCheckOperation::truncate();

        foreach ($collection->chunk(20) as $idschunks) {
            sleep(10);
            foreach ($idschunks->toArray() as $id) {
                $record = $models->execute_kw(
                    $db,
                    $uid,
                    $password,
                    'account.check.operation',
                    'read',
                    [$id],
                    array('fields' => array(
                        "date",
                        "check_id",
                        "operation",
                        "origin_name",
                        "origin",
                        "partner_id",
                        "notes",
                        "smart_search",
                        "id",
                        "display_name",
                        "create_uid",
                        "create_date",
                        "write_uid",
                        "write_date",
                        "__last_update",
                    ))
                );

                AccountCheckOperation::create([
                    "date" => json_encode(!isset($record[0]["date" ]) ? "date"  : $record[0]["date" ]),
                    "check_id" => json_encode(!isset($record[0]["check_id" ]) ? "check_id"  : $record[0]["check_id" ]),
                    "operation" => json_encode(!isset($record[0]["operation" ]) ? "operation"  : $record[0]["operation" ]),
                    "origin_name" => json_encode(!isset($record[0]["origin_name" ]) ? "origin_name"  : $record[0]["origin_name" ]),
                    "origin" => json_encode(!isset($record[0]["origin" ]) ? "origin"  : $record[0]["origin" ]),
                    "partner_id" => json_encode(!isset($record[0]["partner_id" ]) ? "partner_id"  : $record[0]["partner_id" ]),
                    "notes" => json_encode(!isset($record[0]["notes" ]) ? "notes"  : $record[0]["notes" ]),
                    "smart_search" => json_encode(!isset($record[0]["smart_search" ]) ? "smart_search"  : $record[0]["smart_search" ]),
                    "account_check_operation_id" => json_encode(!isset($record[0]["id" ]) ? "account_check_operation_id"  : $record[0]["id" ]),
                    "display_name" => json_encode(!isset($record[0]["display_name" ]) ? "display_name"  : $record[0]["display_name" ]),
                    "create_uid" => json_encode(!isset($record[0]["create_uid" ]) ? "create_uid"  : $record[0]["create_uid" ]),
                    "create_date" => json_encode(!isset($record[0]["create_date" ]) ? "create_date"  : $record[0]["create_date" ]),
                    "write_uid" => json_encode(!isset($record[0]["write_uid" ]) ? "write_uid"  : $record[0]["write_uid" ]),
                    "write_date" => json_encode(!isset($record[0]["write_date" ]) ? "write_date"  : $record[0]["write_date" ]),
                    "__last_update" => json_encode(!isset($record[0]["__last_update" ]) ? "__last_update"  : $record[0]["__last_update" ]),
                ]);
            }
        }
        return $this->successResponse("Finalizado carga de datos a la tabla account_check_operations", 200);
    }
}
