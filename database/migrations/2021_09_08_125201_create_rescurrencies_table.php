<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRescurrenciesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rescurrencies', function (Blueprint $table) {
            $table->id();
              $table->longText("name")->nullable();
              $table->longText("symbol")->nullable();
              $table->longText("rate")->nullable();
              $table->longText("rate_ids")->nullable();
              $table->longText("rounding")->nullable();
              $table->longText("decimal_places")->nullable();
              $table->longText("active")->nullable();
              $table->longText("position")->nullable();
              $table->longText("date")->nullable();
              $table->longText("currency_unit_label")->nullable();
              $table->longText("currency_subunit_label")->nullable();
              $table->longText("inverse_rate")->nullable();
              $table->longText("l10n_ar_afip_code")->nullable();
              $table->longText("smart_search")->nullable();
              $table->longText("rescurrency_id")->nullable();
              $table->longText("display_name")->nullable();
              $table->longText("create_uid" )->nullable();
              $table->longText("create_date" )->nullable();
              $table->longText("write_uid")->nullable();
              $table->longText("write_date")->nullable();
              $table->longText("__last_update")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rescurrencies');
    }
}
