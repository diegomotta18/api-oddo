<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRescurrencyratesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rescurrencyrates', function (Blueprint $table) {
            $table->id();
              $table->longText("name")->nullable();
              $table->longText("rate")->nullable();
              $table->longText("currency_id")->nullable();
              $table->longText("company_id")->nullable();
              $table->longText("inverse_rate")->nullable();
              $table->longText("smart_search")->nullable();
              $table->longText("rescurrencyrate_id")->nullable();
              $table->longText("display_name")->nullable();
              $table->longText("create_uid")->nullable();
              $table->longText("create_date")->nullable();
              $table->longText("write_uid")->nullable();
              $table->longText("write_date")->nullable();
              $table->longText("__last_update")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rescurrencyrates');
    }
}
