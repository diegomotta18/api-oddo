<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStockPickingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stock_pickings', function (Blueprint $table) {
            $table->id();
            $table->longText("name")->nullable();
            $table->longText("origin")->nullable();
            $table->longText("note")->nullable();
            $table->longText("backorder_id")->nullable();
            $table->longText("backorder_ids")->nullable();
            $table->longText("move_type")->nullable();
            $table->longText("state")->nullable();
            $table->longText("group_id")->nullable();
            $table->longText("priority")->nullable();
            $table->longText("scheduled_date")->nullable();
            $table->longText("date")->nullable();
            $table->longText("date_done")->nullable();
            $table->longText("location_id")->nullable();
            $table->longText("location_dest_id")->nullable();
            $table->longText("move_lines")->nullable();
            $table->longText("move_ids_without_package")->nullable();
            $table->longText("has_scrap_move")->nullable();
            $table->longText("picking_type_id")->nullable();
            $table->longText("picking_type_code")->nullable();
            $table->longText("picking_type_entire_packs")->nullable();
            $table->longText("company_id")->nullable();
            $table->longText("user_id")->nullable();
            $table->longText("move_line_ids")->nullable();
            $table->longText("move_line_ids_without_package")->nullable();
            $table->longText("move_line_nosuggest_ids")->nullable();
            $table->longText("move_line_exist")->nullable();
            $table->longText("has_packages")->nullable();
            $table->longText("show_check_availability")->nullable();
            $table->longText("show_mark_as_todo")->nullable();
            $table->longText("show_validate")->nullable();
            $table->longText("use_create_lots")->nullable();
            $table->longText("owner_id")->nullable();
            $table->longText("printed")->nullable();
            $table->longText("is_locked")->nullable();
            $table->longText("product_id")->nullable();
            $table->longText("show_operations")->nullable();
            $table->longText("show_reserved")->nullable();
            $table->longText("show_lots_text")->nullable();
            $table->longText("has_tracking")->nullable();
            $table->longText("immediate_transfer")->nullable();
            $table->longText("package_level_ids")->nullable();
            $table->longText("package_level_ids_details")->nullable();
            $table->longText("partner_id")->nullable();
            $table->longText("state_detail_id")->nullable();
            $table->longText("number_of_packages")->nullable();
            $table->longText("book_id")->nullable();
            $table->longText("vouchers")->nullable();
            $table->longText("voucher_ids")->nullable();
            $table->longText("declared_value")->nullable();
            $table->longText("automatic_declare_value")->nullable();
            $table->longText("book_required")->nullable();
            $table->longText("voucher_required")->nullable();
            $table->longText("next_number")->nullable();
            $table->longText("purchase_id")->nullable();
            $table->longText("sale_id")->nullable();
            $table->longText("carrier_price")->nullable();
            $table->longText("delivery_type")->nullable();
            $table->longText("carrier_id")->nullable();
            $table->longText("volume")->nullable();
            $table->longText("weight")->nullable();
            $table->longText("carrier_tracking_ref")->nullable();
            $table->longText("carrier_tracking_url")->nullable();
            $table->longText("weight_uom_name")->nullable();
            $table->longText("package_ids")->nullable();
            $table->longText("weight_bulk")->nullable();
            $table->longText("shipping_weight")->nullable();
            $table->longText("is_return_picking")->nullable();
            $table->longText("return_label_ids")->nullable();
            $table->longText("require_purchase_order_number")->nullable();
            $table->longText("manual_purchase_order_number")->nullable();
            $table->longText("purchase_order_number")->nullable();
            $table->longText("invoice_ids")->nullable();
            $table->longText("block_manual_lines")->nullable();
            $table->longText("observations")->nullable();
            $table->longText("dispatch_number")->nullable();
            $table->longText("document_type_id")->nullable();
            $table->longText("cot_numero_unico")->nullable();
            $table->longText("cot_numero_comprobante")->nullable();
            $table->longText("cot")->nullable();
            $table->longText("l10n_ar_afip_barcode")->nullable();
            $table->longText("activity_exception_decoration")->nullable();
            $table->longText("activity_exception_icon")->nullable();
            $table->longText("activity_ids")->nullable();
            $table->longText("activity_state")->nullable();
            $table->longText("activity_user_id")->nullable();
            $table->longText("activity_type_id")->nullable();
            $table->longText("activity_date_deadline")->nullable();
            $table->longText("activity_summary")->nullable();
            $table->longText("message_is_follower")->nullable();
            $table->longText("message_follower_ids")->nullable();
            $table->longText("message_partner_ids")->nullable();
            $table->longText("message_channel_ids")->nullable();
            $table->longText("message_ids")->nullable();
            $table->longText("message_unread")->nullable();
            $table->longText("message_unread_counter")->nullable();
            $table->longText("message_needaction")->nullable();
            $table->longText("message_needaction_counter")->nullable();
            $table->longText("message_has_error")->nullable();
            $table->longText("message_has_error_counter")->nullable();
            $table->longText("message_main_attachment_id")->nullable();
            $table->longText("failed_message_ids")->nullable();
            $table->longText("website_message_ids")->nullable();
            $table->longText("message_has_sms_error")->nullable();
            $table->longText("message_attachment_count")->nullable();
            $table->longText("smart_search")->nullable();
            $table->longText("display_name")->nullable();
            $table->longText("create_uid")->nullable();
            $table->longText("create_date")->nullable();
            $table->longText("write_uid")->nullable();
            $table->longText("write_date")->nullable();
            $table->longText("__last_update")->nullable();
            $table->longText("stock_pickings_id")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stock_pickings');
    }
}
