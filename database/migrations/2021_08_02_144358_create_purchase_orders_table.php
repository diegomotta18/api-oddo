<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePurchaseOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_orders', function (Blueprint $table) {
            $table->id();
            $table->longText("name")->nullable();
            $table->longText("origin")->nullable();
            $table->longText("partner_ref")->nullable();
            $table->longText("date_order")->nullable();
            $table->longText("date_approve")->nullable();
            $table->longText("partner_id")->nullable();
            $table->longText("dest_address_id")->nullable();
            $table->longText("currency_id")->nullable();
            $table->longText("state")->nullable();
            $table->longText("order_line")->nullable();
            $table->longText("notes")->nullable();
            $table->longText("invoice_count")->nullable();
            $table->longText("invoice_ids")->nullable();
            $table->longText("invoice_status")->nullable();
            $table->longText("date_planned")->nullable();
            $table->longText("amount_untaxed")->nullable();
            $table->longText("amount_tax")->nullable();
            $table->longText("amount_total")->nullable();
            $table->longText("fiscal_position_id")->nullable();
            $table->longText("payment_term_id")->nullable();
            $table->longText("product_id")->nullable();
            $table->longText("user_id")->nullable();
            $table->longText("company_id")->nullable();
            $table->longText("currency_rate")->nullable();
            $table->longText("incoterm_id")->nullable();
            $table->longText("picking_count")->nullable();
            $table->longText("picking_ids")->nullable();
            $table->longText("picking_type_id")->nullable();
            $table->longText("default_location_dest_id_usage")->nullable();
            $table->longText("group_id")->nullable();
            $table->longText("is_shipped")->nullable();
            $table->longText("internal_notes")->nullable();
            $table->longText("force_invoiced_status")->nullable();
            $table->longText("force_delivered_status")->nullable();
            $table->longText("delivery_status")->nullable();
            $table->longText("with_returns")->nullable();
            $table->longText("activity_exception_decoration")->nullable();
            $table->longText("activity_exception_icon")->nullable();
            $table->longText("activity_ids")->nullable();
            $table->longText("activity_state")->nullable();
            $table->longText("activity_user_id")->nullable();
            $table->longText("activity_type_id")->nullable();
            $table->longText("activity_date_deadline")->nullable();
            $table->longText("activity_summary")->nullable();
            $table->longText("message_is_follower")->nullable();
            $table->longText("message_follower_ids")->nullable();
            $table->longText("message_partner_ids")->nullable();
            $table->longText("message_channel_ids")->nullable();
            $table->longText("message_ids")->nullable();
            $table->longText("message_unread")->nullable();
            $table->longText("message_unread_counter")->nullable();
            $table->longText("message_needaction")->nullable();
            $table->longText("message_needaction_counter")->nullable();
            $table->longText("message_has_error")->nullable();
            $table->longText("message_has_error_counter")->nullable();
            $table->longText("message_main_attachment_id")->nullable();
            $table->longText("failed_message_ids")->nullable();
            $table->longText("website_message_ids")->nullable();
            $table->longText("message_has_sms_error")->nullable();
            $table->longText("message_attachment_count")->nullable();
            $table->longText("smart_search")->nullable();
            $table->longText("access_url")->nullable();
            $table->longText("access_token")->nullable();
            $table->longText("access_warning")->nullable();
            $table->longText("purchase_order_id")->nullable();
            $table->longText("display_name")->nullable();
            $table->longText("create_uid")->nullable();
            $table->longText("create_date")->nullable();
            $table->longText("write_uid")->nullable();
            $table->longText("write_date")->nullable();
            $table->longText("__last_update")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchase_orders');
    }
}
