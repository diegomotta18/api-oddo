<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductTemplateAttributeLinesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_template_attribute_lines', function (Blueprint $table) {
            $table->id();
            $table->text("active")->nullable();
            $table->text("product_tmpl_id")->nullable();
            $table->text("attribute_id")->nullable();
            $table->text("value_ids")->nullable();
            $table->text("product_template_value_ids")->nullable();
            $table->text("smart_search")->nullable();
            $table->text("product_template_attribute_line_id")->nullable();
            $table->text("display_name")->nullable();
            $table->text("create_uid")->nullable();
            $table->text("create_date")->nullable();
            $table->text("write_uid")->nullable();
            $table->text("write_date")->nullable();
            $table->text("__last_update")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_template_attribute_lines');
    }
}
