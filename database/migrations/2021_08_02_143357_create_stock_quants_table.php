<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStockQuantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stock_quants', function (Blueprint $table) {
            $table->id();
            $table->longText("product_id")->nullable();
            $table->longText("product_tmpl_id")->nullable();
            $table->longText("product_uom_id")->nullable();
            $table->longText("company_id")->nullable();
            $table->longText("location_id")->nullable();
            $table->longText("lot_id")->nullable();
            $table->longText("package_id")->nullable();
            $table->longText("owner_id")->nullable();
            $table->longText("quantity")->nullable();
            $table->longText("reserved_quantity")->nullable();
            $table->longText("in_date")->nullable();
            $table->longText("tracking")->nullable();
            $table->longText("on_hand")->nullable();
            $table->longText("removal_date")->nullable();
            $table->longText("smart_search")->nullable();
            $table->longText("stock_quant_id")->nullable();
            $table->longText("display_name")->nullable();
            $table->longText("create_uid")->nullable();
            $table->longText("create_date")->nullable();
            $table->longText("write_uid")->nullable();
            $table->longText("write_date")->nullable();
            $table->longText("__last_update")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stock_quants');
    }
}
