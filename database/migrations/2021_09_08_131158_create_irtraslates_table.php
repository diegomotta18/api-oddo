<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIrtraslatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('irtraslates', function (Blueprint $table) {
            $table->id();
            $table->longText("name")->nullable();
            $table->longText("res_id")->nullable();
            $table->longText("lang")->nullable();
            $table->longText("type")->nullable();
            $table->longText("src")->nullable();
            $table->longText("value")->nullable();
            $table->longText("module")->nullable();
            $table->longText("state")->nullable();
            $table->longText("comments")->nullable();
            $table->longText("transifex_url")->nullable();
            $table->longText("smart_search")->nullable();
            $table->longText("irtraslate_id")->nullable();
            $table->longText("display_name")->nullable();
            $table->longText("__last_update")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('irtraslates');
    }
}
