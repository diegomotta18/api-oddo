<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePurchaseOrderLinesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_order_lines', function (Blueprint $table) {
            $table->id();
            $table->longText("name")->nullable() ;
            $table->longText("sequence")->nullable() ;
            $table->longText("product_qty")->nullable() ;
            $table->longText("product_uom_qty")->nullable() ;
            $table->longText("date_planned")->nullable() ;
            $table->longText("taxes_id")->nullable() ;
            $table->longText("product_uom")->nullable() ;
            $table->longText("product_uom_category_id")->nullable() ;
            $table->longText("product_id")->nullable() ;
            $table->longText("product_type")->nullable() ;
            $table->longText("price_unit")->nullable() ;
            $table->longText("price_subtotal")->nullable() ;
            $table->longText("price_total")->nullable() ;
            $table->longText("price_tax")->nullable() ;
            $table->longText("account_analytic_id")->nullable() ;
            $table->longText("analytic_tag_ids")->nullable() ;
            $table->longText("company_id")->nullable() ;
            $table->longText("state")->nullable() ;
            $table->longText("invoice_lines")->nullable() ;
            $table->longText("qty_invoiced")->nullable() ;
            $table->longText("qty_received")->nullable() ;
            $table->longText("qty_received_manual")->nullable() ;
            $table->longText("partner_id")->nullable() ;
            $table->longText("currency_id")->nullable() ;
            $table->longText("date_order")->nullable() ;
            $table->longText("display_type")->nullable() ;
            $table->longText("qty_received_method")->nullable() ;
            $table->longText("move_ids")->nullable() ;
            $table->longText("orderpoint_id")->nullable() ;
            $table->longText("move_dest_ids")->nullable() ;
            $table->longText("propagate_date")->nullable() ;
            $table->longText("propagate_date_minimum_delta")->nullable() ;
            $table->longText("propagate_cancel")->nullable() ;
            $table->longText("discount")->nullable() ;
            $table->longText("order_id")->nullable() ;
            $table->longText("invoice_status")->nullable() ;
            $table->longText("invoice_qty")->nullable() ;
            $table->longText("qty_to_invoice")->nullable() ;
            $table->longText("sale_order_id")->nullable() ;
            $table->longText("sale_line_id")->nullable() ;
            $table->longText("delivery_status")->nullable() ;
            $table->longText("vouchers")->nullable() ;
            $table->longText("qty_on_voucher")->nullable() ;
            $table->longText("qty_returned")->nullable() ;
            $table->longText("smart_search")->nullable() ;
            $table->longText("purchase_order_line_id")->nullable() ;
            $table->longText("display_name")->nullable() ;
            $table->longText("create_uid")->nullable() ;
            $table->longText("create_date")->nullable() ;
            $table->longText("write_uid")->nullable() ;
            $table->longText("write_date")->nullable() ;
            $table->longText("__last_update")->nullable() ;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchase_order_lines');
    }
}
