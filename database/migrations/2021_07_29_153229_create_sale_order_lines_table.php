<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSaleOrderLinesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sale_order_lines', function (Blueprint $table) {
            $table->id();
            $table->text("name")->nullable();
            $table->text("sequence")->nullable();
            $table->text("invoice_lines")->nullable();
            $table->text("invoice_status")->nullable();
            $table->text("price_unit")->nullable();
            $table->text("price_subtotal")->nullable();
            $table->text("price_tax")->nullable();
            $table->text("price_total")->nullable();
            $table->text("price_reduce")->nullable();
            $table->text("tax_id")->nullable();
            $table->text("price_reduce_taxinc")->nullable();
            $table->text("price_reduce_taxexcl")->nullable();
            $table->text("discount")->nullable();
            $table->text("product_id")->nullable();
            $table->text("product_template_id")->nullable();
            $table->text("product_updatable")->nullable();
            $table->text("product_uom_qty")->nullable();
            $table->text("product_uom")->nullable();
            $table->text("product_uom_category_id")->nullable();
            $table->text("product_custom_attribute_value_ids")->nullable();
            $table->text("product_no_variant_attribute_value_ids")->nullable();
            $table->text("qty_delivered")->nullable();
            $table->text("qty_delivered_manual")->nullable();
            $table->text("qty_to_invoice")->nullable();
            $table->text("qty_invoiced")->nullable();
            $table->text("untaxed_amount_invoiced")->nullable();
            $table->text("untaxed_amount_to_invoice")->nullable();
            $table->text("salesman_id")->nullable();
            $table->text("currency_id")->nullable();
            $table->text("company_id")->nullable();
            $table->text("order_partner_id")->nullable();
            $table->text("analytic_tag_ids")->nullable();
            $table->text("is_expense")->nullable();
            $table->text("is_downpayment")->nullable();
            $table->text("state")->nullable();
            $table->text("customer_lead")->nullable();
            $table->text("display_type")->nullable();
            $table->text("product_can_modify_prices")->nullable();
            $table->text("exception_ids")->nullable();
            $table->text("exceptions_summary")->nullable();
            $table->text("ignore_exception")->nullable();
            $table->text("sale_order_option_ids")->nullable();
            $table->text("is_configurable_product")->nullable();
            $table->text("product_template_attribute_value_ids")->nullable();
            $table->text("purchase_line_ids")->nullable();
            $table->text("purchase_line_count")->nullable();
            $table->text("product_packaging")->nullable();
            $table->text("route_id")->nullable();
            $table->text("move_ids")->nullable();
            $table->text("product_type")->nullable();
            $table->text("virtual_available_at_date")->nullable();
            $table->text("scheduled_date")->nullable();
            $table->text("free_qty_today")->nullable();
            $table->text("qty_available_today")->nullable();
            $table->text("warehouse_id")->nullable();
            $table->text("qty_to_deliver")->nullable();
            $table->text("is_mto")->nullable();
            $table->text("display_qty_widget")->nullable();
            $table->text("is_delivery")->nullable();
            $table->text("product_qty")->nullable();
            $table->text("recompute_delivery_price")->nullable();
            $table->text("report_price_unit")->nullable();
            $table->text("price_unit_with_tax")->nullable();
            $table->text("report_price_subtotal")->nullable();
            $table->text("report_price_net")->nullable();
            $table->text("report_tax_id")->nullable();
            $table->text("vat_tax_id")->nullable();
            $table->text("report_price_reduce")->nullable();
            $table->text("subscription_id")->nullable();
            $table->text("qty_delivered_method")->nullable();
            $table->text("project_id")->nullable();
            $table->text("task_id")->nullable();
            $table->text("is_service")->nullable();
            $table->text("analytic_line_ids")->nullable();
            $table->text("all_qty_delivered")->nullable();
            $table->text("quantity_returned")->nullable();
            $table->text("delivery_status")->nullable();
            $table->text("smart_search")->nullable();
            $table->text("sale_order_line_id")->nullable();
            $table->text("display_name")->nullable();
            $table->text("create_uid")->nullable();
            $table->text("create_date")->nullable();
            $table->text("write_uid")->nullable();
            $table->text("write_date")->nullable();
            $table->text("__last_update")->nullable();
            $table->text("order_id")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sale_order_lines');
    }
}
