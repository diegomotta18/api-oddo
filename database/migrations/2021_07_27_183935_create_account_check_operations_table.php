<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAccountCheckOperationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('account_check_operations', function (Blueprint $table) {
            $table->id();
            $table->longText("date")->nullable();
            $table->longText("check_id")->nullable();
            $table->longText("operation")->nullable();
            $table->longText("origin_name")->nullable();
            $table->longText("origin")->nullable();
            $table->longText("partner_id")->nullable();
            $table->longText("notes")->nullable();
            $table->longText("smart_search")->nullable();
            $table->longText("account_check_operation_id")->nullable();
            $table->longText("display_name")->nullable();
            $table->longText("create_uid")->nullable();
            $table->longText("create_date")->nullable();
            $table->longText("write_uid")->nullable();
            $table->longText("write_date")->nullable();
            $table->longText("__last_update")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('account_check_operations');
    }
}
