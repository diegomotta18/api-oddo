<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStockValuationLayersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stock_valuation_layers', function (Blueprint $table) {
            $table->id();
            $table->longText("active")->nullable();
            $table->longText("company_id")->nullable();
            $table->longText("product_id")->nullable();
            $table->longText("categ_id")->nullable();
            $table->longText("product_tmpl_id")->nullable();
            $table->longText("quantity")->nullable();
            $table->longText("uom_id")->nullable();
            $table->longText("currency_id")->nullable();
            $table->longText("unit_cost")->nullable();
            $table->longText("value")->nullable();
            $table->longText("remaining_qty")->nullable();
            $table->longText("remaining_value")->nullable();
            $table->longText("description")->nullable();
            $table->longText("stock_valuation_layer_id")->nullable();
            $table->longText("stock_valuation_layer_ids")->nullable();
            $table->longText("stock_move_id")->nullable();
            $table->longText("account_move_id")->nullable();
            $table->longText("smart_search")->nullable();
            $table->longText("stock_valuation_layer_app_id")->nullable();
            $table->longText("display_name")->nullable();
            $table->longText("create_uid")->nullable();
            $table->longText("create_date")->nullable();
            $table->longText("write_uid")->nullable();
            $table->longText("write_date")->nullable();
            $table->longText("__last_update")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stock_valuation_layers');
    }
}
