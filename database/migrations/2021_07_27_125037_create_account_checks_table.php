<?php
declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAccountChecksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('account_checks', function (Blueprint $table) {
            $table->id();
            $table->longText("operation_ids")->nullable();
            $table->longText("name")->nullable();
            $table->string("number")->nullable();
            $table->string("checkbook_id")->nullable();
            $table->string("issue_check_subtype")->nullable();
            $table->string("type")->nullable();
            $table->longText("partner_id")->nullable();
            $table->longText("first_partner_id")->nullable();
            $table->string("state")->nullable();
            $table->string("issue_date")->nullable();
            $table->string("owner_vat")->nullable();
            $table->string("owner_name")->nullable();
            $table->longText("bank_id")->nullable();
            $table->string("amount")->nullable();
            $table->string("amount_company_currency")->nullable();
            $table->longText("currency_id")->nullable();
            $table->string("payment_date")->nullable();
            $table->longText("journal_id")->nullable();
            $table->longText("company_id")->nullable();
            $table->longText("company_currency_id")->nullable();
            $table->string("activity_exception_decoration")->nullable();
            $table->string("activity_exception_icon")->nullable();
            $table->string("activity_ids")->nullable();
            $table->string("activity_state")->nullable();
            $table->string("activity_user_id")->nullable();
            $table->string("activity_type_id")->nullable();
            $table->string("activity_date_deadline")->nullable();
            $table->string("activity_summary")->nullable();
            $table->longText("message_is_follower")->nullable();
            $table->longText("message_follower_ids")->nullable();
            $table->longText("message_partner_ids")->nullable();
            $table->longText("message_channel_ids")->nullable();
            $table->longText("message_ids")->nullable();
            $table->string("message_unread")->nullable();
            $table->string("message_unread_counter")->nullable();
            $table->string("message_needaction")->nullable();
            $table->string("message_needaction_counter")->nullable();
            $table->string("message_has_error")->nullable();
            $table->string("message_has_error_counter")->nullable();
            $table->string("message_main_attachment_id")->nullable();
            $table->longText("failed_message_ids")->nullable();
            $table->longText("website_message_ids")->nullable();
            $table->string("message_has_sms_error")->nullable();
            $table->string("message_attachment_count")->nullable();
            $table->string("smart_search")->nullable();
            $table->string("account_check_id")->nullable();
            $table->string("display_name")->nullable();
            $table->longText("create_uid")->nullable();
            $table->string("create_date")->nullable();
            $table->longText("write_uid")->nullable();
            $table->string("write_date")->nullable();
            $table->string("__last_update")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('account_checks');
    }

}
