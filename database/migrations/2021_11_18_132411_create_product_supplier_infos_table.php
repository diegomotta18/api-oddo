<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductSupplierInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_supplier_infos', function (Blueprint $table) {
            $table->id();
            $table->longText( "name")->nullable();
            $table->longText(  "product_name")->nullable();
            $table->longText(  "product_code")->nullable();
            $table->longText(  "sequence" )->nullable();
            $table->longText(  "product_uom" )->nullable();
            $table->longText( "min_qty" )->nullable();
            $table->longText(  "price" )->nullable();
            $table->longText(  "company_id" )->nullable();
            $table->longText(  "currency_id" )->nullable();
            $table->longText(  "date_start" )->nullable();
            $table->longText("date_end" )->nullable();
            $table->longText("product_id" )->nullable();
            $table->longText("product_tmpl_id" )->nullable();
            $table->longText("product_variant_count" )->nullable();
            $table->longText("delay" )->nullable();
            $table->longText("last_date_price_updated" )->nullable();
            $table->longText("replenishment_cost_rule_id" )->nullable();
            $table->longText("net_price" )->nullable();
            $table->longText("discount" )->nullable();
            $table->longText("smart_search" )->nullable();
            $table->longText("product_supplier_infos_id" )->nullable();
            $table->longText("display_name" )->nullable();
            $table->longText("create_uid" )->nullable();
            $table->longText("create_date" )->nullable();
            $table->longText("write_uid" )->nullable();
            $table->longText("write_date" )->nullable();
            $table->longText("__last_update" )->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_supplier_infos');
    }
}
