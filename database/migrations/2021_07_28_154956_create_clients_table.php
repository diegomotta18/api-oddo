<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->engine = 'innodb row_format=dynamic';
            $table->id();
            $table->text("name")->nullable();
            $table->text("display_name")->nullable();
            $table->text("date")->nullable();
            $table->text("title")->nullable();
            $table->text("parent_id")->nullable();
            $table->text("parent_name")->nullable();
            $table->text("child_ids")->nullable();
            $table->text("ref")->nullable();
            $table->text("lang")->nullable();
            $table->text("active_lang_count")->nullable();
            $table->text("tz")->nullable();
            $table->text("tz_offset")->nullable();
            $table->text("same_vat_partner_id")->nullable();
            $table->text("bank_ids")->nullable();
            $table->text("website")->nullable();
            $table->text("comment")->nullable();
            $table->text("category_id")->nullable();
            $table->text("credit_limit")->nullable();
            $table->text("employee")->nullable();
            $table->text("function")->nullable();
            $table->text("type")->nullable();
            $table->text("street")->nullable();
            $table->text("street2")->nullable();
            $table->text("zip")->nullable();
            $table->text("city")->nullable();
            $table->text("state_id")->nullable();
            $table->text("country_id")->nullable();
            $table->text("partner_latitude")->nullable();
            $table->text("partner_longitude")->nullable();
            $table->text("email")->nullable();
            $table->text("email_formatted")->nullable();
            $table->text("phone")->nullable();
            $table->text("mobile")->nullable();
            $table->text("is_company")->nullable();
            $table->text("company_type")->nullable();
            $table->text("company_id")->nullable();
            $table->text("color")->nullable();
            $table->text("user_ids")->nullable();
            $table->text("partner_share")->nullable();
            $table->text("contact_address")->nullable();
            $table->text("commercial_partner_id")->nullable();
            $table->text("commercial_company_name")->nullable();
            $table->text("company_name")->nullable();
            $table->text("self")->nullable();
            $table->text("im_status")->nullable();
            $table->text("activity_exception_decoration")->nullable();
            $table->text("activity_exception_icon")->nullable();
            $table->text("email_normalized")->nullable();
            $table->text("is_blacklisted")->nullable();
            $table->text("message_bounce")->nullable();
            $table->text("channel_ids")->nullable();
            $table->text("user_id")->nullable();
            $table->text("contact_address_complete")->nullable();
            $table->text("signup_valid")->nullable();
            $table->text("signup_url")->nullable();
            $table->text("active")->nullable();
            $table->text("calendar_last_notif_ack")->nullable();
            $table->text("email_bounced")->nullable();
            $table->text("tracking_emails_count")->nullable();
            $table->text("email_score")->nullable();
            $table->text("failed_message_ids")->nullable();
            $table->text("ocn_token")->nullable();
            $table->text("partner_gid")->nullable();
            $table->text("additional_info")->nullable();
            $table->text("phone_sanitized")->nullable();
            $table->text("phone_blacklisted")->nullable();
            $table->text("property_product_pricelist")->nullable();
            $table->text("industry_id")->nullable();
            $table->text("secondary_industry_ids")->nullable();
            $table->text("message_has_sms_error")->nullable();
            $table->text("property_stock_customer")->nullable();
            $table->text("property_stock_supplier")->nullable();
            $table->text("picking_warn")->nullable();
            $table->text("picking_warn_msg")->nullable();
            $table->text("message_attachment_count")->nullable();
            $table->text("activity_ids")->nullable();
            $table->text("activity_state")->nullable();
            $table->text("activity_user_id")->nullable();
            $table->text("activity_type_id")->nullable();
            $table->text("activity_date_deadline")->nullable();
            $table->text("activity_summary")->nullable();
            $table->text("is_seo_optimized")->nullable();
            $table->text("is_published")->nullable();
            $table->text("can_publish")->nullable();
            $table->text("website_url")->nullable();
            $table->text("website_published")->nullable();
            $table->text("visitor_ids")->nullable();
            $table->text("credit")->nullable();
            $table->text("debit")->nullable();
            $table->text("debit_limit")->nullable();
            $table->text("total_invoiced")->nullable();
            $table->text("currency_id")->nullable();
            $table->text("journal_item_count")->nullable();
            $table->text("property_account_payable_id")->nullable();
            $table->text("property_account_receivable_id")->nullable();
            $table->text("property_account_position_id")->nullable();
            $table->text("property_payment_term_id")->nullable();
            $table->text("property_supplier_payment_term_id")->nullable();
            $table->text("ref_company_ids")->nullable();
            $table->text("has_unreconciled_entries")->nullable();
            $table->text("last_time_entries_checked")->nullable();
            $table->text("invoice_ids")->nullable();
            $table->text("contract_ids")->nullable();
            $table->text("bank_account_count")->nullable();
            $table->text("trust")->nullable();
            $table->text("invoice_warn")->nullable();
            $table->text("invoice_warn_msg")->nullable();
            $table->text("supplier_rank")->nullable();
            $table->text("customer_rank")->nullable();
            $table->text("team_id")->nullable();
            $table->text("opportunity_ids")->nullable();
            $table->text("meeting_ids")->nullable();
            $table->text("opportunity_count")->nullable();
            $table->text("opportunity_count_ids")->nullable();
            $table->text("meeting_count")->nullable();
            $table->text("task_ids")->nullable();
            $table->text("task_count")->nullable();
            $table->text("website_description")->nullable();
            $table->text("website_short_description")->nullable();
            $table->text("property_account_receivable_ids")->nullable();
            $table->text("property_account_payable_ids")->nullable();
            $table->text("property_account_position_ids")->nullable();
            $table->text("property_payment_term_ids")->nullable();
            $table->text("property_supplier_payment_term_ids")->nullable();
            $table->text("property_product_pricelist_ids")->nullable();
            $table->text("online_partner_vendor_name")->nullable();
            $table->text("online_partner_bank_account")->nullable();
            $table->text("payment_token_ids")->nullable();
            $table->text("payment_token_count")->nullable();
            $table->text("property_purchase_currency_id")->nullable();
            $table->text("purchase_order_count")->nullable();
            $table->text("supplier_invoice_count")->nullable();
            $table->text("purchase_warn")->nullable();
            $table->text("purchase_warn_msg")->nullable();
            $table->text("l10n_latam_identification_type_id")->nullable();
            $table->text("vat")->nullable();
            $table->text("sale_order_count")->nullable();
            $table->text("sale_order_ids")->nullable();
            $table->text("sale_warn")->nullable();
            $table->text("sale_warn_msg")->nullable();
            $table->text("payment_next_action_date")->nullable();
            $table->text("unreconciled_aml_ids")->nullable();
            $table->text("unpaid_invoices")->nullable();
            $table->text("total_due")->nullable();
            $table->text("total_overdue")->nullable();
            $table->text("followup_status")->nullable();
            $table->text("followup_level")->nullable();
            $table->text("payment_responsible_id")->nullable();
            $table->text("default_supplierinfo_discount")->nullable();
            $table->text("property_delivery_carrier_id")->nullable();
            $table->text("sale_type")->nullable();
            $table->text("require_purchase_order_number")->nullable();
            $table->text("subscription_count")->nullable();
            $table->text("gross_income_jurisdiction_ids")->nullable();
            $table->text("start_date")->nullable();
            $table->text("estado_padron")->nullable();
            $table->text("imp_ganancias_padron")->nullable();
            $table->text("imp_iva_padron")->nullable();
            $table->text("integrante_soc_padron")->nullable();
            $table->text("monotributo_padron")->nullable();
            $table->text("actividad_monotributo_padron")->nullable();
            $table->text("empleador_padron")->nullable();
            $table->text("actividades_padron")->nullable();
            $table->text("impuestos_padron")->nullable();
            $table->text("last_update_padron")->nullable();
            $table->text("arba_alicuot_ids")->nullable();
            $table->text("drei")->nullable();
            $table->text("default_regimen_ganancias_id")->nullable();
            $table->text("smart_search")->nullable();
            $table->text("client_id")->nullable();
            $table->text("create_uid")->nullable();
            $table->text("create_date")->nullable();
            $table->text("write_uid")->nullable();
            $table->text("write_date")->nullable();
            $table->text("__last_update")->nullable();
            $table->text("x_pricelist_m2m")->nullable();
            //LO NUEVO
            $table->text("internal_code")->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
    }
}
