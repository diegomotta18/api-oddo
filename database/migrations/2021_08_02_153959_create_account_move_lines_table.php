<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAccountMoveLinesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('account_move_lines', function (Blueprint $table) {
            $table->id();
            $table->longText("move_name")->nullable();
            $table->longText("date")->nullable();
            $table->longText("ref")->nullable();
            $table->longText("parent_state")->nullable();
            $table->longText("journal_id")->nullable();
            $table->longText("company_id")->nullable();
            $table->longText("company_currency_id")->nullable();
            $table->longText("country_id")->nullable();
            $table->longText("account_internal_type")->nullable();
            $table->longText("account_root_id")->nullable();
            $table->longText("sequence")->nullable();
            $table->longText("name")->nullable();
            $table->longText("quantity")->nullable();
            $table->longText("price_unit")->nullable();
            $table->longText("discount")->nullable();
            $table->longText("debit")->nullable();
            $table->longText("credit")->nullable();
            $table->longText("balance")->nullable();
            $table->longText("amount_currency")->nullable();
            $table->longText("price_subtotal")->nullable();
            $table->longText("price_total")->nullable();
            $table->longText("reconciled")->nullable();
            $table->longText("blocked")->nullable();
            $table->longText("date_maturity")->nullable();
            $table->longText("currency_id")->nullable();
            $table->longText("partner_id")->nullable();
            $table->longText("product_uom_id")->nullable();
            $table->longText("product_id")->nullable();
            $table->longText("statement_line_id")->nullable();
            $table->longText("statement_id")->nullable();
            $table->longText("tax_line_id")->nullable();
            $table->longText("tax_group_id")->nullable();
            $table->longText("tax_base_amount")->nullable();
            $table->longText("tax_exigible")->nullable();
            $table->longText("tag_ids")->nullable();
            $table->longText("tax_audit")->nullable();
            $table->longText("amount_residual")->nullable();
            $table->longText("amount_residual_currency")->nullable();
            $table->longText("full_reconcile_id")->nullable();
            $table->longText("matched_debit_ids")->nullable();
            $table->longText("matched_credit_ids")->nullable();
            $table->longText("analytic_line_ids")->nullable();
            $table->longText("recompute_tax_line")->nullable();
            $table->longText("display_type")->nullable();
            $table->longText("is_rounding_line")->nullable();
            $table->longText("exclude_from_invoice_tab")->nullable();
            $table->longText("always_set_currency_id")->nullable();
            $table->longText("move_attachment_ids")->nullable();
            $table->longText("financial_amount_residual")->nullable();
            $table->longText("financial_amount")->nullable();
            $table->longText("move_id")->nullable();
            $table->longText("account_id")->nullable();
            $table->longText("reconcile_model_id")->nullable();
            $table->longText("payment_id")->nullable();
            $table->longText("tax_ids")->nullable();
            $table->longText("tax_repartition_line_id")->nullable();
            $table->longText("analytic_account_id")->nullable();
            $table->longText("analytic_tag_ids")->nullable();
            $table->longText("l10n_latam_document_type_id")->nullable();
            $table->longText("l10n_latam_price_unit")->nullable();
            $table->longText("l10n_latam_price_subtotal")->nullable();
            $table->longText("l10n_latam_price_net")->nullable();
            $table->longText("l10n_latam_tax_ids")->nullable();
            $table->longText("purchase_line_id")->nullable();
            $table->longText("is_anglo_saxon_line")->nullable();
            $table->longText("predict_from_name")->nullable();
            $table->longText("predict_override_default_account")->nullable();
            $table->longText("expected_pay_date")->nullable();
            $table->longText("internal_note")->nullable();
            $table->longText("next_action_date")->nullable();
            $table->longText("user_id")->nullable();
            $table->longText("purchase_subscription_id")->nullable();
            $table->longText("sale_line_ids")->nullable();
            $table->longText("asset_id")->nullable();
            $table->longText("followup_line_id")->nullable();
            $table->longText("followup_date")->nullable();
            $table->longText("payment_group_ids")->nullable();
            $table->longText("payment_group_matched_amount")->nullable();
            $table->longText("product_can_modify_prices")->nullable();
            $table->longText("tax_settlement_move_id")->nullable();
            $table->longText("tax_state")->nullable();
            $table->longText("subscription_id")->nullable();
            $table->longText("subscription_start_date")->nullable();
            $table->longText("subscription_end_date")->nullable();
            $table->longText("subscription_mrr")->nullable();
            $table->longText("move_line_ids")->nullable();
            $table->longText("smart_search")->nullable();
            $table->longText("account_move_line_id")->nullable();
            $table->longText("display_name")->nullable();
            $table->longText("create_uid")->nullable();
            $table->longText("create_date")->nullable();
            $table->longText("write_uid")->nullable();
            $table->longText("write_date")->nullable();
            $table->longText("__last_update")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('account_move_lines');
    }
}
